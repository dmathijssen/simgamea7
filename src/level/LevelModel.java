package level;

import block.BlockController;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by gjoosen on 22/05/15.
 */
public class LevelModel {

    private List<List<String>> map;

    private List<List<BlockController>> blocks;
    private int mapWidth, mapHeight;

    //collision
    private List<List<Boolean>> collisionMap;


    public LevelModel(String map, String mapCollision) {
        //read the map
        this.readMapBlocks(map);
        this.readCollisionMap(mapCollision);
    }

    private void readCollisionMap(String mapCollision) {
        this.collisionMap = new ArrayList<>();
        try(BufferedReader reader = new BufferedReader(new FileReader(mapCollision))){
            reader.lines().forEach(e -> {
                List<Boolean> list = new ArrayList<Boolean>();
                for(String s: e.split(",")){
                    if(!s.equals("0")){
                        list.add(true);
                    }else{
                        list.add(false);
                    }
                }
                this.collisionMap.add(list);
            });
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void readMap(String mapFile){
        try(BufferedReader reader = new BufferedReader(new FileReader(mapFile))){
            //line1 = width
            //line2 = height
            this.map = new ArrayList<>();
            reader.lines().forEach(e -> {
                if (e.startsWith("width=")) {
                    String[] widthS = e.split("=");
                    this.mapWidth = Integer.valueOf(widthS[1]);
                } else if (e.startsWith("height=")) {
                    String[] heightS = e.split("=");
                    this.mapHeight = Integer.valueOf(heightS[1]);
                } else {
                    List<String> tileList = new ArrayList<String>();
                    String[] parts = e.split(",");
                    for (String s : parts) {
                        System.out.println(s);
                        tileList.add(s);
                    }
                    map.add(tileList);
                }

            });
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void readMapBlocks(String mapFile){
        try(BufferedReader reader = new BufferedReader(new FileReader(mapFile))){
            this.blocks = new ArrayList<>();
            reader.lines().forEach(e -> {
                if (e.startsWith("width=")) {
                    String[] widthS = e.split("=");
                    this.mapWidth = Integer.valueOf(widthS[1]);
                } else if (e.startsWith("height=")) {
                    String[] heightS = e.split("=");
                    this.mapHeight = Integer.valueOf(heightS[1]);
                } else {
                    //block code
                    List<BlockController> blocks = new ArrayList<BlockController>();
                    String[] parts = e.split(",");
                    for (String s : parts) {
                        if (!s.equals("")) {
                            blocks.add(new BlockController(Integer.valueOf(s)));
                        }
                    }
                    this.blocks.add(blocks);
                }
            });
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<List<String>> getMap() {
        return map;
    }

    public List<List<BlockController>> getBlocks() {
        return blocks;
    }

    public List<List<Boolean>> getCollisionMap() {
        return collisionMap;
    }
    
    public int getLevelWidth(){
    	return mapWidth * 32;
    }
    
    public int getLevelHeight(){
    	return mapHeight *32;
    }
    
    
}
