package level;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
/**
 * Created by gjoosen on 22/05/15.
 */
public class LevelController {

    private LevelModel levelModel;

    private List<BufferedImage> images;

    public LevelController(){
        this.levelModel = new LevelModel("res/map/map-10.txt", "res/map/collision-map-10.txt");

        this.images = new ArrayList<>();
        //file this, hard coded for now
        this.getImages("res/generic_platformer_tiles.png");
    }

    private void getImages(String spriteSheet){
        try {
            BufferedImage image = ImageIO.read(new File(spriteSheet));
            int imageWidth = image.getWidth();
            int imageHeight = image.getHeight();
            int offset = 0;

            //imageheight
            for(int j = 0; j < imageHeight; j+=32){
                //height
                if(j + j/32*offset + 32 <= imageHeight){
                    //imagewidth
                    for(int i = 0; i <imageWidth; i+=32){
                        //row
                        if(i + i/32*offset + 32 <= imageWidth){
                            images.add(image.getSubimage(i + i/32*offset, j + j/32*offset, 32, 32));
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * draw the level to the Graphics2D object.
     * @param g2 the Graphics2D to draw on.
     */
    public void draw(Graphics2D g2){
        final int[] y = {0};
        final int[] x = {0};
        this.levelModel.getBlocks().stream().forEach(e -> {
            e.stream().forEach(number ->{
                if(!number.equals("")){
                    g2.drawImage(this.images.get(number.getNumber()-1), null, x[0]*32, y[0]*32);
                }
                x[0] +=1;
            });
            y[0] +=1;
            x[0] = 0;
        });
    }

    public void drawCollisionMap(Graphics2D g2){
        final int[] x = {0};
        final int[] y = {0};
        g2.setColor(Color.PINK);
        this.levelModel.getCollisionMap().stream().forEach(e -> {
            e.stream().forEach(b -> {
                if(b){
                    g2.drawRect(x[0]*32, y[0]*32, 32, 32);
                }
                x[0] += 1;
            });
            y[0]+=1;
            x[0] = 0;
        });
    }

    public LevelModel getLevelModel() {
        return levelModel;
    }
    
    public int getLevelWidth(){
    	return levelModel.getLevelWidth();
    }
    
    public int getLevelHeight(){
    	return levelModel.getLevelHeight();
    }
}
