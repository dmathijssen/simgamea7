package block;

/**
 * Created by gjoosen on 28/05/15.
 */
public class BlockController {

    private int number;
    private boolean collision;

    public BlockController(int number) {
        this.number = number;
        //598,599,600,601,602
        int[] collidenumbers = {58};
        for(int a: collidenumbers){
            if(number == a){
                collision = true;
                return;
            }
        }

    }

    public int getNumber() {
        return number;
    }

    public boolean isCollision() {
        return collision;
    }
}
