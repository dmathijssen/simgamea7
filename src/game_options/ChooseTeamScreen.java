package game_options;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;

import menu.MenuView;

@SuppressWarnings("serial")
public class ChooseTeamScreen extends OptionScreen {

	private BufferedImage tdmBackground;
	private List<BufferedImage> players = new ArrayList<BufferedImage>();

	private String message = "";
	protected List<Point2D> playerPosition = new ArrayList<Point2D>();
	private String[] playerResources = { "res/selection/player1.png",
			"res/selection/player2.png", "res/selection/player3.png",
			"res/selection/player4.png" };

	protected int selectedPlayer = 0;
	private int amountPlayers;
	MenuView view;
	ArrayList<Integer> teams = new ArrayList<>();

	public ChooseTeamScreen(MenuView view, int players) {
		super(view, 2);
		this.amountPlayers = players;
		this.view = view;
		loadImages();
//		System.out.println(players);

		repaint();
	}

	public void loadImages() {
		try {
			tdmBackground = ImageIO.read(new File(
					"res/selection/tdm-background.png"));
			for (int i = 0; i < amountPlayers; i++) {
				players.add(ImageIO.read(new File(playerResources[i])));
				playerPosition.add(new Point2D.Float(930, 420 + (i * 130)));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void select() {
		if (!message.equals("")) {
			message = "";
			repaint();
			return;
		}
		for (int i = 0; i < playerPosition.size(); i++) {
			Point2D p = playerPosition.get(i);
			System.out.println(p.getX());
			if (p.getX() == 930) {
				message = "nog niet alle spelers hebben een team gekozen!";
				teams.clear();
				repaint();
				return;
			} else if (p.getX() == 700) {
				teams.add(1);
			} else {
				teams.add(2);
			}
		}
		view.launchGame(teams);
	}

	@Override
	protected void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		int width = getWidth();
		int height = getHeight();

		// make background black
		g2.drawImage(tdmBackground, null, 0, 0);

		// surround the selected image with a green rectangle
		g2.setColor(Color.green);
		g2.setFont(new Font(Font.DIALOG, Font.BOLD, 20));
		g2.setStroke(new BasicStroke(10));
		g2.drawRect((int) playerPosition.get(selectedPlayer).getX(),
				selectedPlayer * 130 + 420, 100, 100);

		// images of the selections. The size of each image is(500,300)
		for (int i = 0; i < players.size(); i++) {
			g2.drawImage(players.get(i), null, (int) playerPosition.get(i)
					.getX(), (int) playerPosition.get(i).getY());
		}

		if (!(message.equals(""))) {
			g2.setColor(Color.white);
			g2.fill(new Rectangle(width / 2 - 350, height / 2 - 250, 700, 500));
			g2.setColor(Color.green);
			g2.setStroke(new BasicStroke(10));
			g2.drawRect(width / 2 - 350, height / 2 - 250, 700, 500);
			g2.setFont(new Font(Font.DIALOG, Font.BOLD, 25));
			g2.setColor(Color.black);
			g2.drawString(message, width / 2 - 250, height / 2);

		}
	}

	public void moveUpSelection() {
		if (selectedPlayer > 0)
			selectedPlayer--;
		repaint();
	}

	public void moveDownSelection() {
		if (selectedPlayer < amountPlayers - 1)
			selectedPlayer++;
		repaint();
	}

	public void moveRightSelection() {
		Point2D p = playerPosition.get(selectedPlayer);
		if (p.getX() <= 930)
			p.setLocation(p.getX() + 230, p.getY());
		repaint();
	}

	public void moveLeftSelection() {
		Point2D p = playerPosition.get(selectedPlayer);
		if (p.getX() >= 930)
			p.setLocation(p.getX() - 230, p.getY());
		repaint();
	}

	@Override
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_DOWN:
			moveDownSelection();
			break;
		case KeyEvent.VK_UP:
			moveUpSelection();
			break;
		}

	}

}
