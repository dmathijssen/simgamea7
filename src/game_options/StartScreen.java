package game_options;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.*;

import arena.NoControllersFoundException;
import arena.WiiMoteController;
import menu.MenuView;
import options.SoundOptions;

public class StartScreen extends OptionScreen {

    protected BufferedImage background;
	protected BufferedImage playButton;
	protected BufferedImage optionButton;
	protected BufferedImage arrow;
	protected BufferedImage arrow2;
	protected int arrowPositionX;
	protected int arrowPositionY;
	protected int arrowPositionX2;
	private int countDown = 0;

	public StartScreen(MenuView view) {
		super(view, -1);
		arrowPositionX = 960 - 95;
		arrowPositionX2 = 960 + 73;
		arrowPositionY = 540 - 78;

        try {
            background = ImageIO.read(new File("res/selection/jungla.png"));
			playButton = ImageIO.read(new File("res/button-play.png"));
			optionButton = ImageIO.read(new File("res/button-settings.png"));
			arrow = ImageIO.read(new File("res/arrow.png"));
			arrow2 = ImageIO.read(new File("res/arrow2.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}

        SoundOptions.readSettings();

	}

	public void moveUpArrow() {
		if (countDown > 0) {
			arrowPositionY -= 155;
			countDown--;
			repaint();
		}

	}

	public void moveDownArrow() {
		if (countDown <= 0) {
			arrowPositionY += 155;
			countDown++;
			repaint();
		}

	}

	@Override
	public void select() {
        switch (countDown){
            case 0:
                //spel
                view.setPanel(new AmountOfPlayersScreen(this.view));
                break;
            case 1:
                //options
                view.setPanel(new SoundOptions(this.view,3));
                break;
        }
		

	}

	@Override
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_DOWN:
			moveDownArrow();
			break;
		case KeyEvent.VK_UP:
			moveUpArrow();
			break;
		}

	}

	@Override
	protected void paintComponent(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		g2d.drawImage(background, null, 0, 0);
		Rectangle bg = new Rectangle(0, 0, 1920, 1080);
		g2d.setColor(Color.black);
		g2d.fill(bg);
		g2d.setColor(Color.yellow);
		g2d.setFont(new Font(Font.DIALOG, Font.BOLD, 20));

		g2d.drawImage(background, null, 0, 0);
		g2d.drawImage(playButton, null, (int) Toolkit.getDefaultToolkit()
				.getScreenSize().getWidth() / 2 - 50, (int) Toolkit
				.getDefaultToolkit().getScreenSize().getHeight() / 2 - 100);
		g2d.drawImage(optionButton, null, (int) Toolkit.getDefaultToolkit()
				.getScreenSize().getWidth() / 2 - 50, (int) Toolkit
				.getDefaultToolkit().getScreenSize().getHeight() / 2 + 50);
		g2d.drawImage(arrow, null, arrowPositionX, arrowPositionY);
		g2d.drawImage(arrow2, null, arrowPositionX2, arrowPositionY);

		g2d.drawImage(background, null, 0, 0);
		g2d.drawImage(playButton, null, 960 - 50, 540 - 100);
		g2d.drawImage(optionButton, null, 960 - 50, 540 + 50);
		g2d.drawImage(arrow, null, arrowPositionX, arrowPositionY);
		g2d.drawImage(arrow2, null, arrowPositionX2, arrowPositionY);

	}

}
