package game_options;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import menu.MenuView;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamPanel;
import com.github.sarxos.webcam.WebcamResolution;

/**
 * Created by imegumii on 6/5/15.
 */
public class WebcamCaptureView extends OptionScreen {

	Webcam webcam;

	BufferedImage todraw;
	BufferedImage bg;

	int playerNumber = 1;
	int toTake;
	MenuView view;

	// font
	private Font font = null;

	public WebcamCaptureView(MenuView view, int toTake) {
		super(view, -1);
		super.revalidate();
		this.toTake = toTake;
		this.view = view;
		webcam = Webcam.getDefault();
		webcam.setViewSize(WebcamResolution.VGA.getSize());

		WebcamPanel panel = new WebcamPanel(webcam);
		this.add(panel);
		try {
			bg = ImageIO
					.read(new File("res/selection/players-backgrounds.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}

		// load 8bit font
		Font customFont = null;
		try {
			customFont = Font.createFont(Font.TRUETYPE_FONT, new File(
					"res/fonts/04B_30__.TTF"));
			font = customFont.deriveFont(Font.PLAIN, 30f);
			GraphicsEnvironment ge = GraphicsEnvironment
					.getLocalGraphicsEnvironment();
			ge.registerFont(font);
		} catch (FontFormatException | IOException e) {
			e.printStackTrace();
		}

	}

	private void writeToFile() {
		try {
			BufferedImage b = createResizedCopy(todraw, 150, 100, false);
			modAlpha(b, 50); 
			ImageIO.write(b, "PNG", new File("res/webcampictures/player"
					+ (playerNumber - 1) + ".png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private BufferedImage createResizedCopy(Image originalImage,
			int scaledWidth, int scaledHeight, boolean preserveAlpha) {
		int imageType = preserveAlpha ? BufferedImage.TYPE_INT_RGB
				: BufferedImage.TYPE_INT_ARGB;
		BufferedImage scaledBI = new BufferedImage(scaledWidth, scaledHeight,
				imageType);
		Graphics2D g = scaledBI.createGraphics();
		if (preserveAlpha) {
			g.setComposite(AlphaComposite.Src);
		}
		g.drawImage(originalImage, 0, 0, scaledWidth, scaledHeight, null);
		g.dispose();
		return scaledBI;
	}

	public void modAlpha(BufferedImage modMe, double modAmount) {
		//
		for (int x = 0; x < modMe.getWidth(); x++) {
			for (int y = 0; y < modMe.getHeight(); y++) {

				int argb = modMe.getRGB(x, y); // always returns TYPE_INT_ARGB
				int alpha = (argb >> 24) & 0xff; // isolate alpha

				alpha *= modAmount; // similar distortion to tape saturation
									// (has scrunching effect, eliminates
									// clipping)
				alpha &= 0xff; // keeps alpha in 0-255 range

				argb &= 0x00ffffff; // remove old alpha info
				argb |= (alpha << 24); // add new alpha info
				modMe.setRGB(x, y, argb);
			}
		}
	}

	@Override
	protected void paintComponent(Graphics graphics) {
		Graphics2D g2d = (Graphics2D) graphics;
		g2d.drawImage(bg, null, 0, 0);
		if (todraw != null) {
			g2d.drawImage(todraw, null, getWidth() / 2 - todraw.getWidth() / 2,
					getHeight() / 2 - todraw.getHeight() / 2);
		}
		g2d.setFont(font);
		g2d.setColor(Color.white);
		g2d.drawString(
				"Make photo of Player " + String.valueOf(playerNumber),
				getWidth()/2 - 250, getHeight() / 2 +150);

	}

	public boolean spacePressed() {
		if (playerNumber < toTake) {
			todraw = webcam.getImage();
			writeToFile();
			playerNumber++;
			repaint();
			return false;
		} else {
			todraw = webcam.getImage();
			writeToFile();
			playerNumber++;
			select();
			
		}
		return true;
	}

	@Override
	public void select() {
		webcam.close();
		if (toTake == 2) {
			view.launchGame(null);
		} else {
			SelectGameModeScreen gamemodeScreen = new SelectGameModeScreen(
					view, toTake);
			view.setPanel(gamemodeScreen);
		}

	}

	@Override
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_SPACE:
			spacePressed();
			break;
		}

	}
}
