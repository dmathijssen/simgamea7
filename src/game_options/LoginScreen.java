package game_options;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.imageio.ImageIO;
import javax.swing.*;

import login.LoginDialog;
import menu.MenuView;

public class LoginScreen extends OptionScreen {

	private BufferedImage loginBackground;
	private BufferedImage goButton;
	private ArrayList<BufferedImage> players = new ArrayList<BufferedImage>();

	// key: playernumber, value database id
	private HashMap<Integer, Integer> playerIDCollection = new HashMap<Integer, Integer>();
	private String[] playerResources = { "res/selection/player1.png",
			"res/selection/player2.png", "res/selection/player3.png",
			"res/selection/player4.png" };
	private int amountOfPlayers;
	private boolean isGoSelected = false;

	// player who is currently selected
	private int selectedPlayer = 0;

	private JDialog dialog;
	private boolean lastTimeValidLogin = false;

	public LoginScreen(MenuView view, int players) {
		super(view, players);
		this.amountOfPlayers = players;
		loadImages();
		repaint();
	}

	public void loadImages() {
		try {
			loginBackground = ImageIO.read(new File(
					"res/selection/players-backgrounds.png"));
			goButton = ImageIO.read(new File("res/selection/choose-team.jpg"));
			for (int i = 0; i < amountOfPlayers; i++) {
				players.add(ImageIO.read(new File(playerResources[i])));

				playerIDCollection.put(i, -1);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;

		// draw background
		g2.drawImage(loginBackground, null, 0, 0);

		// draw players
		g2.setColor(Color.white);
		for (int i = 0; i < amountOfPlayers; i++) {
			g2.drawImage(players.get(i), null, 240 + i * 480, 50);
			g2.setFont(new Font("Verdana", Font.CENTER_BASELINE, 45));
			g2.drawString("player " + (i + 1), 160 + i * 480, 200);
		}

		// draw selection rectangle	and login text
		String loginText = "Logged in";
		for (int i = 0; i < amountOfPlayers; i++) {
			g2.setColor(Color.green);
			if(playerIDCollection.get(i) == -1){
				g2.setColor(Color.red);
				loginText = "login with essteling";
			}else{
				loginText = "Logged in";
			}
			g2.setStroke(new BasicStroke(10));
			g2.fillRect(75 + i * 480, 500, 375, 100);
			
			//draw login text
			g2.setColor(Color.white);
			
			g2.setFont(new Font("Verdana", Font.BOLD, 30));
			g2.drawString(loginText, 100 + i * 480, 550);
		}

		// draw highlighted rectangle
		g2.setColor(Color.white);
		if (!isGoSelected)
			g2.drawRect(75 + selectedPlayer * 480,500, 375, 100);
		else
			g2.drawRect(560, 850, 800, 85);

		// draw go button
		g2.drawImage(goButton, null, 560, 850);
	}

	@Override
	public void select() {
		if (lastTimeValidLogin) {
			lastTimeValidLogin = false;
			return;
		}
		if (isGoSelected) {
			view.setPlayerCollection(this.playerIDCollection);
			view.setPanel(new WebcamCaptureView(view, amountOfPlayers));
			
		} else {
			// login dialog
			this.dialog = new LoginDialog(this);
		}

	}

	public void moveRightSelection() {
		if (selectedPlayer < amountOfPlayers-1) {
			selectedPlayer++;
		}
		repaint();
	}

	public void moveLeftSelection() {
		if (selectedPlayer > 0) {
			selectedPlayer--;
		}
		repaint();
	}

	public void moveDownSelection() {
		isGoSelected = true;
		repaint();
	}

	public void moveUpSelection() {

		isGoSelected = false;
		repaint();
	}

	@Override
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_UP:
			moveUpSelection();
			break;

		case KeyEvent.VK_DOWN:
			moveDownSelection();
			break;

		default:
			repaint();
		}
	}

	public void setDataBaseID(int dataBaseID) {
		playerIDCollection.replace(selectedPlayer, dataBaseID);
	}

	public void validLogin() {
		this.dialog.dispose();
		lastTimeValidLogin = true;
	}
}
