package game_options;

import java.awt.Graphics;
import java.awt.event.KeyEvent;

import javax.swing.JPanel;

import menu.MenuView;

public abstract class OptionScreen extends JPanel {
	private int currentPosition = 0;
	private int amountPositions;
	protected MenuView view;
	
	public OptionScreen(MenuView view, int amountPositions) {
		this.amountPositions =amountPositions; 
		this.view = view;
	}
	
	public void moveRightSelection(){
		if (!(currentPosition >= amountPositions)) {
			currentPosition++;
			repaint();
		}
	}
	
	public void moveLeftSelection(){
		if (!(currentPosition <= 0)) {
			currentPosition--;
			repaint();
		}
	}
	
	public abstract void select();
	
	
	/*
	 * this method handles other pressed keys
	 */
	public abstract void keyPressed(KeyEvent e);

	protected abstract void paintComponent(Graphics g);
	
	public int getCurrentPosition(){
		return currentPosition;
	}

}
