package game_options;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import arena.NoControllersFoundException;
import arena.WiiMoteController;
import menu.MenuView;

@SuppressWarnings("serial")
public class AmountOfPlayersScreen extends OptionScreen{

	BufferedImage twoPlayers;
	BufferedImage threePlayers;
	BufferedImage fourPlayers;
	private BufferedImage playerBackground;
	MenuView view;

	public AmountOfPlayersScreen(MenuView view) {
		super(view,2);
		loadImages();
		this.view = view;
		repaint();
	}

	public void loadImages() {
		try
		{
			twoPlayers = ImageIO.read(new File("res/selection/2players.png"));
			threePlayers = ImageIO.read(new File("res/selection/3players.png"));
			fourPlayers = ImageIO.read(new File("res/selection/4players.png"));
			playerBackground = ImageIO.read(new File("res/selection/players-backgrounds.png"));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public void select() {
		view.setPlayers(getCurrentPosition()+2);
		try {
            if(!System.getProperty("os.name").equals("Mac OS X")){
                WiiMoteController wmc = new WiiMoteController(getCurrentPosition()+2, null);
                view.setWmc(wmc);
            }
		} catch (NoControllersFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		view.setPanel(new LoginScreen(view, getCurrentPosition()+2));
	}

	@Override
	protected void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		int width = getWidth();
		int height = getHeight();

		// draw background
		g2.drawImage(playerBackground, null, 0, 0);

		// images of the selections. The size of each image is(500,300)
		g2.drawImage(twoPlayers, null, width / 2 - 900, height / 2 - 100);
		g2.drawImage(threePlayers, null, width / 2 - 250, height / 2 - 100);
		g2.drawImage(fourPlayers, null, width / 2 + 400, height / 2 - 100);

		// show options
		g2.setFont(new Font(Font.DIALOG, Font.BOLD, 20));
		g2.setStroke(new BasicStroke(5));

		switch (getCurrentPosition()) {
		// when the first option is selected
		case 0:
			g2.setColor(Color.green);
			g2.drawString("2 spelers", width / 2 - 700, height / 2 - 150);
			g2.drawRect(width / 2 - 900, height / 2 - 100, 500, 300);
			g2.setColor(Color.white);
			g2.drawString("3 spelers", width / 2 - 50, height / 2 - 150);
			g2.drawString("4 spelers", width / 2 + 650, height / 2 - 150);
			break;

		// when the second option is selected
		case 1:
			g2.setColor(Color.white);
			g2.drawString("2 spelers", width / 2 - 700, height / 2 - 150);
			g2.setColor(Color.green);
			g2.drawRect(width / 2 - 250, height / 2 - 100, 500, 300);
			g2.drawString("3 spelers", width / 2 - 50, height / 2 - 150);
			g2.setColor(Color.white);
			g2.drawString("4 spelers", width / 2 + 650, height / 2 - 150);
			break;

		// when the third option is selected
		case 2:
			g2.setColor(Color.white);
			g2.drawString("2 spelers", width / 2 - 700, height / 2 - 150);
			g2.drawString("3 spelers", width / 2 - 50, height / 2 - 150);
			g2.setColor(Color.green);
			g2.drawString("4 spelers", width / 2 + 650, height / 2 - 150);
			g2.drawRect(width / 2 + 400, height / 2 - 100, 500, 300);
			break;

		default:
			break;
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

}
