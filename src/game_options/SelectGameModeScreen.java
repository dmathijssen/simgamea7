package game_options;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import menu.MenuView;

@SuppressWarnings("serial")
public class SelectGameModeScreen extends OptionScreen{

	private BufferedImage freeforall;
	private BufferedImage tdm;
	private BufferedImage playerBackground;
	MenuView view;
	private int players;

	public SelectGameModeScreen(MenuView view, int players) {
		super(view, 1);
		loadImages();
		this.view = view;
		this.players = players;
		repaint();
	}

	public void loadImages() {
		try
		{
			freeforall = ImageIO.read(new File("res/selection/freeforall.png"));
			tdm = ImageIO.read(new File("res/selection/team-deathmatch.png"));
			playerBackground = ImageIO.read(new File(
					"res/selection/players-backgrounds.png"));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public void select() {
		if(getCurrentPosition() == 0){
			view.launchGame(null);
		}else{
			view.setPanel(new ChooseTeamScreen(view, players));
			
		}
	}

	/*
	 * this method draws the screen where the player can select a gamemode
	 */
	public void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		int width = getWidth();
		int height = getHeight();

		// draw background
		g2.drawImage(playerBackground, null, 0, 0);

		// draw options
		g2.drawImage(freeforall, null, width / 2 - 550, height / 2 - 100);
		g2.drawImage(tdm, null, width / 2 + 50, height / 2 - 100);

		// draw and highlight selected option
		g2.setFont(new Font(Font.DIALOG, Font.BOLD, 20));
		g2.setStroke(new BasicStroke(5));

		switch (getCurrentPosition()) {
		// highlight first option
		case 0:
			g2.setColor(Color.green);
			g2.drawString("free for all", width / 2 - 375, height / 2 - 150);
			g2.drawRect(width / 2 - 550, height / 2 - 100, 500, 300);
			g2.setColor(Color.white);
			g2.drawString("team deathmatch", width / 2 + 220, height / 2 - 150);
			break;

		// highlight second option
		case 1:
			g2.setColor(Color.white);
			g2.drawString("free for all", width / 2 - 375, height / 2 - 150);
			g2.setColor(Color.green);
			g2.drawString("team deathmatch", width / 2 + 220, height / 2 - 150);
			g2.drawRect(width / 2 + 50, height / 2 - 100, 500, 300);
			break;

		default:
			break;
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
	}
}
