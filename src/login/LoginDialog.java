package login;

import game_options.LoginScreen;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import db.Database;


/**
 * Created by gjoosen on 11/06/15.
 */
public class LoginDialog extends JDialog {

    private JTextField login;
    private JPasswordField password;
    private JButton button;
    private LoginScreen loginScreen;

    public LoginDialog(LoginScreen loginScreen) {
        super();
        this.loginScreen = loginScreen;

        super.setSize(200, 150);
        super.setLocation(loginScreen.getWidth()/2-100, loginScreen.getHeight()/2-75);


        JPanel content = new JPanel();
        content.setLayout(new BoxLayout(content, BoxLayout.Y_AXIS));

        this.login = new JTextField();
        this.password = new JPasswordField();
        this.button = new JButton("Login");
        button.setContentAreaFilled(false);
        content.add(new JLabel("Username: "));
        content.add(login);
        content.add(new JLabel("Password: "));
        content.add(password);
        content.add(button);

        this.password.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyCode() == KeyEvent.VK_ENTER){
                	login();   
                }
            }
        });
        
        this.login.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyCode() == KeyEvent.VK_ENTER){
                	login();   
                }
            }
        });
        
        button.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyCode() == KeyEvent.VK_ENTER){
                	login();   
                }
            }
        });
        
        button.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mousePressed(java.awt.event.MouseEvent evt)
            {
                login();
            }
        });

        super.add(content);

        super.setVisible(true);
    }

    @SuppressWarnings("deprecation")
	private void login(){
    	
    	int dataBaseID  = -1;
    	Database db = new Database();
    	try {
    		String stringPassword = "";
    		for(char c: password.getPassword()){
    			stringPassword += c;
    		}
			dataBaseID = db.playerLogin(login.getText(), stringPassword);
			loginScreen.setDataBaseID(dataBaseID);
			loginScreen.repaint();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
        //authenticate
        if(dataBaseID != -1){
        	JOptionPane.showMessageDialog(null, "succesfully logged in", "", JOptionPane.WARNING_MESSAGE);
        	super.dispose();
            this.loginScreen.validLogin();
        }else{
            JOptionPane.showMessageDialog(null, "invalid login", "The username/password combination is unknown!", JOptionPane.WARNING_MESSAGE);
        }
    }
}
