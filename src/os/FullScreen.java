package os;

import javax.swing.*;
import java.awt.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by gjoosen on 29/05/15.
 */
public class FullScreen
{
    public static void enableFullScreen( JFrame frame )
    {
        if( System.getProperties().getProperty( "os.name" ).equals( "Mac OS X" ) )
        {
            try
            {
                Class<?> c = Class.forName( "com.apple.eawt.FullScreenUtilities" );
                Method m = c.getMethod( "setWindowCanFullScreen", Window.class, Boolean.TYPE );
                m.invoke( c, frame, true );
                System.out.println( "full screen!" );
            }
            catch( Exception e )
            {
                e.printStackTrace();
            }
        }
        else if( System.getProperties().getProperty( "os.name" ).startsWith( "Windows" ) ){
		       //No special methods needed because "windows" == "master race"
        }else if( System.getProperties().getProperty( "os.name" ).equals( "Linux" ) )
        {
            Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
            frame.setSize( d );
            frame.setLocation(0, 0);
            GraphicsDevice g = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
            g.setFullScreenWindow(frame);

        }
    }
}
