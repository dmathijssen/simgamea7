package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Database {
  private Connection connect = null;   //db connection
  private PreparedStatement preparedStatement = null;   //preparedstatement for filling db with acts
  
  //database values
  private static final String HOST = "jdbc:mysql://filmlist.nl:3306/filmlist_arenagame";
  private static final String USER = "filmlist_java";
  private static final String PASS = "1]FG@eoPf8Iu";
  
  public Database(){

  }
  
  /*
   * @param gebruikersID: The id of the user in the database
   * @param punten: Amount of points the player earned that will be added to the total point count in the database
   * 
   * Save the points in the database to the user
   */
  public void savePoints(int gebruikersId, int punten) throws Exception {
    try {
      Class.forName("com.mysql.jdbc.Driver");
      connect = DriverManager.getConnection(HOST,USER,PASS);

	  //add extra points to gebruiker
	  preparedStatement = connect.prepareStatement("UPDATE gebruikers SET punten = punten + ? WHERE gebruikers_id = ? ");
	  preparedStatement.setInt(1, punten);
	  preparedStatement.setInt(2, gebruikersId);
      preparedStatement.executeUpdate();
            
    } catch (Exception e) {
      throw e;
    } finally {
    	if (preparedStatement != null) {
            preparedStatement.close();
          }

          if (connect != null) {
            connect.close();
          }
    }

  }
  
  /*
   * @param username: The username of the player
   * @param password: The password of the player
   * 
   * Check if player and password are in the database. If the player and password are correct, return the gebruikersID
   * else (if the player doesn't exist or password is wrong) return -1
   */
  public int playerLogin(String username, String password) throws Exception {
	    try {
	      Class.forName("com.mysql.jdbc.Driver");
	      connect = DriverManager.getConnection(HOST,USER,PASS);
	      
    	  //insert act data into database
    	  preparedStatement = connect.prepareStatement("SELECT gebruikers_id FROM gebruikers WHERE gebruikers_naam = ? AND wachtwoord = ?");
    	  preparedStatement.setString(1, username);
    	  preparedStatement.setString(2, password);
          
          ResultSet rs = preparedStatement.executeQuery();
          while (rs.next()) {
          	return rs.getInt("gebruikers_id");	
          }
	            
	    } catch (Exception e) {
	      throw e;
	    } finally {
	    	if (preparedStatement != null) {
	            preparedStatement.close();
	          }

	          if (connect != null) {
	            connect.close();
	          }
	          
	    }
	    //if no results where found
	    return -1;
  }
}