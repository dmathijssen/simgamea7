package entities;

import arena.ArenaModel;
import arena.SpriteGetter;
import sounds.Sound;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Player extends Draw implements Comparable<Player>
{
    private final int SPEED = 200;
    private final int JUMPSPEED = 250;
    private final double GRAVITY_BASE = 100;

    private Aim aim;
    private int playerNumber;

    private BufferedImage image;

    private BufferedImage spriteSheet;

    private boolean movingLeft;
    private boolean movingRight;
    private boolean jumping;
    private boolean canJump;
    private boolean duck;
    private boolean isFalling;

    private List<Arrow> arrows;
    private int numberOfArrows = 4;
    private double gravityPlayer = 100;
    private int team;
    private int jumpingCount = 0;
    private int levelWidth, levelHeight;
    private int playerDatabaseId;

    //font
    private Font customFontArrowCount = null;

    //arrow image
    private BufferedImage arrowImage;

    //kills
    private int kills = 0;

    //killed
    private boolean dead;

    private int startX, startY;

    private SpriteGetter sg;

    public Player( int playerNumber, int x, int y, BufferedImage bufferedImage, double rotation, double scale, ArenaModel arenaModel )
    {
        super( x, y, bufferedImage, rotation, scale, arenaModel );
        this.spriteSheet = bufferedImage;
        this.playerNumber = playerNumber;
        movingLeft = false;
        movingRight = false;
        jumping = false;
        duck = false;
        canJump = true;
        isFalling = false;

        sg = new SpriteGetter( playerNumber );


        this.startX = x;
        this.startY = y;

        //arrows
        this.arrows = new ArrayList<>();

        try
        {
            this.arrowImage = ImageIO.read( new File( "res/shoot-arrow.png" ) );
            //            image = ImageIO.read( new File( "res/webcampictures/player" + playerNumber + ".png" ) );
        }
        catch( IOException e )
        {
            e.printStackTrace();
        }

        //load 8bit font
        Font customFont = null;
        try
        {
            customFont = Font.createFont( Font.TRUETYPE_FONT, new File( "res/fonts/04B_30__.TTF" ) );
            customFontArrowCount = customFont.deriveFont( Font.PLAIN, 20f );
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            ge.registerFont( customFontArrowCount );
        }
        catch( FontFormatException | IOException e )
        {
            e.printStackTrace();
        }
    }

    public void setLevelSizes( int width, int height )
    {
        this.levelWidth = width;
        this.levelHeight = height;
    }

    public void walkLeft()
    {
        // updatePosition(-5, 0);
        movingLeft = true;
        movingRight = false;
    }

    public void resetwalkLeft()
    {
        movingLeft = false;
    }

    public void resetwalkingRight()
    {
        movingRight = false;
    }

    public void resetJumping()
    {
        jumping = false;
        isFalling = true;
        jumpingCount = 0;
    }

    public void resetCrouching()
    {
        duck = false;
    }

    public void walkRight()
    {
        // updatePosition(5, 0);
        movingRight = true;
        movingLeft = false;
    }

    public void jump()
    {
        // updatePosition(0, -5);
        if( canJump )
        {
            jumping = true;
            isFalling = false;
            canJump = false;
            duck = false;
        }
    }

    public void chrouch()
    {
        //		updatePosition(0, 5);
        duck = true;
        jumping = false;
    }

    public void rest()
    {
        movingRight = false;
        movingLeft = false;
        duck = false;

    }

    /**
     * fire an entity
     */
    public void fire()
    {
        if( !this.isDead() )
        {
            try
            {
                //            Point2D point = MouseInfo.getPointerInfo().getLocation();
                Point2D point = aim.getPosition();
                point.setLocation( point.getX() + 16, point.getY() - 16 );
                if( this.numberOfArrows > 0 )
                {
                    arrows.add(new Arrow((int) super.x, (int) super.y - 5, ImageIO.read(new File("res/shoot-arrow.png")), super.arenaModel, 500, point, this ) );
                    this.numberOfArrows--;
                    Sound.playShootArrow();
                }
            }
            catch( IOException e )
            {
                e.printStackTrace();
            }
        }
    }

    public void aim( Point2D aimpoint )
    {
        // draw objects

        //            Aim aim = new Aim( ( int ) aimpoint.getX(), ( int ) aimpoint.getY(), ImageIO.read( new File( "res/player0/aim.png" ) ), 0, 1, arenaModel );
        aim.setPosition( aimpoint );
        arenaModel.getAim( this ).setPosition( aimpoint );
        //            arenaModel.setAim(this, aim);

    }

    private void setSpriteAccordingly()
    {
        long time = System.currentTimeMillis();

        if( movingRight )
        {
            if( jumping )
            {
                super.setBufferedImage( sg.getJumpingRightSprite( true ) );
            }
            else if( isFalling )
            {
                super.setBufferedImage( sg.getJumpingRightSprite( false ) );
            }
            else
            {
                super.setBufferedImage( sg.getWalkingRightSprite() );
            }
        }

        if( movingLeft )
        {
            if( jumping )
            {
                super.setBufferedImage( sg.getJumpingLeftSprite( true ) );
            }
            else if( isFalling )
            {
                super.setBufferedImage( sg.getJumpingLeftSprite( false ) );
            }
            else
            {
                super.setBufferedImage( sg.getWalkingLeftSprite() );
            }
        }

//        System.out.println( "Time taken: " + ( System.currentTimeMillis() - time ) );
//        System.out.println( "Time taken: " + ( System.currentTimeMillis() - time ) );
    }

    public void move( double delta )
    {
        double xMove = 0, yMove = 0;
        if( movingLeft )
        {
            if( !checkCollision( -SPEED * delta, 0 ) )
            {
                xMove = -SPEED * delta;
            }
        }
        if( movingRight )
        {
            if( !checkCollision( SPEED * delta, 0 ) )
            {
                xMove = SPEED * delta;
            }
        }
        if( jumping )
        {
        	if(jumpingCount == 0){
        		Sound.playPlayerJump();
        	}
            if( !checkCollision( 0, -JUMPSPEED * delta ) )
            {
                yMove = -JUMPSPEED * delta;
            }
            if( jumpingCount > 700 )
            {
                jumping = false;
                isFalling = true;
            }
            jumpingCount += delta * 1000;
        }
        if( duck )
        {
            if( !checkCollision( 0, SPEED * delta ) )
            {
                yMove = SPEED * delta;
            }
        }
        if( !checkCollision( 0, gravityPlayer * delta ) && !jumping )
        {
            yMove = gravityPlayer * delta;
            gravityPlayer += 500 * delta; //tweak this to liking
        }
        else
        {
            gravityPlayer = GRAVITY_BASE;
        }
        this.setSpriteAccordingly();
        this.updatePosition( xMove, yMove );
    }

    private boolean checkCollision( double xMove, double yMove )
    {
        double newXPosition = this.getX() + xMove;
        double newYPosition = this.getY() + yMove;

        //check collision
        List<List<Boolean>> collisionMap = super.arenaModel.getController().getLevelController().getLevelModel().getCollisionMap();
        Rectangle2D player = new Rectangle2D.Double( newXPosition, newYPosition, super.bufferedImage.getWidth(), super.bufferedImage.getHeight() );

        final boolean[] collision = { false };
        final int[] y = { 0 };
        final int[] x = { 0 };
        collisionMap.forEach( e -> {
            e.stream().forEach( b -> {
                //check collision
                if( b )
                {
                    Rectangle2D rectangle2 = new Rectangle2D.Double( x[ 0 ] * 32, y[ 0 ] * 32, 32, 32 );
                    Rectangle2D rect = new Rectangle2D.Double( x[ 0 ] * 32, y[ 0 ] * 32, 22, 10 );
                    if(rectangle2.intersects( player )){
                        collision[ 0 ] = true;
                    }

                    if(rect.intersects( player.getX(), player.getY() + 5, player.getWidth(), player.getHeight())){
                        jumpingCount = 0;
                        canJump = true;
                        isFalling = false;
                    }
                }
                x[ 0 ] += 1;
            } );
            x[ 0 ] = 0;
            y[ 0 ] += 1;
        } );
        if( collision[ 0 ] )
        {
            return collision[ 0 ];
        }

        //check player collision
        super.arenaModel.getPlayersMap().keySet().forEach( e -> {
            if( e != this && !e.isDead() )
            {
                Rectangle2D rectangle2D = new Rectangle2D.Double( e.x, e.y, e.width, e.height );
                if( rectangle2D.intersects( player ) )
                {
                    collision[ 0 ] = true;
                }
            }
        } );
        return collision[ 0 ];
    }

    public void setAim( Aim aim )
    {
        this.aim = aim;
    }

    public void updateArrows( double delta )
    {
        //shallow copy
        ConcurrentLinkedQueue<Arrow> list = new ConcurrentLinkedQueue<>( this.arrows );

        list.stream().forEach( arrow -> {
            arrow.setDelta( delta );
            //if borderCheck passes, update arrow movement
            if( arrow.borderCheck( levelWidth, levelHeight ) )
            {
                arrow.updatePosition( arrow.getxMove() * delta * arrow.getVelocity(), arrow.getyMove() * delta * arrow.getVelocity() );
            }
            // arrow.updatePosition( arrow.getxMove() * delta * arrow.getVelocity(), arrow.getyMove() * delta * arrow.getVelocity() );

        } );
    }

    @Override public void draw( Graphics2D g2 )
    {
        super.draw( g2 );
        this.arrows.stream().forEach( e -> e.draw( g2 ) );

        //draw number of arrows on top of the player

        //draw the arrow image
        AffineTransform transform = new AffineTransform();
        transform.translate( ( super.x + super.bufferedImage.getWidth() / 2 ) - ( g2.getFontMetrics().stringWidth( this.numberOfArrows + "" ) + 7 ), super.y );
        transform.rotate( Math.toRadians( -90 ) );
        transform.scale( 0.5, 0.5 );
        g2.drawImage( this.arrowImage, transform, null );

        //draw text
        g2.setColor( Color.white );
        g2.setFont( customFontArrowCount );


        //draw an ellipse with team color
        if( team == 1 )
        {
            g2.setColor( Color.red );
        }
        if( team == 2 )
        {
            g2.setColor( Color.blue );
        }
        if( team != 0 )
        {
            g2.setStroke( new BasicStroke( 5 ) );
            g2.fill( new Ellipse2D.Double( ( int ) x, ( int ) y - 20, getWidth(), 20 ) );
            g2.setStroke( new BasicStroke( 1 ) );
            g2.setColor( Color.white );
            g2.drawString( this.numberOfArrows + "", ( int ) ( super.x + super.bufferedImage.getWidth() / 2 ) - ( g2.getFontMetrics().stringWidth( this.numberOfArrows + "" ) / 2 ), ( int ) super.y - 20 );
        }
        else
        {
            g2.setColor( Color.white );
            g2.drawString( this.numberOfArrows + "", ( int ) ( super.x + super.bufferedImage.getWidth() / 2 ) - ( g2.getFontMetrics().stringWidth( this.numberOfArrows + "" ) / 2 ), ( int ) super.y );
        }

        g2.drawString( ( "P" + ( playerNumber + 1 ) ), ( int ) ( super.x + super.bufferedImage.getWidth() / 2 ) - ( g2.getFontMetrics().stringWidth( "P" + ( playerNumber + 1 ) ) / 2 ), ( int ) super.y + 50 );


        // draw number of arrows on top of the player
        //        g2.setColor( Color.white );
        //        g2.drawString( this.numberOfArrows + "", ( int ) ( super.x + super.bufferedImage.getWidth() / 2 ) - ( g2.getFontMetrics().stringWidth( this.numberOfArrows + "" ) / 2 ), ( int ) super.y + 20 );
        //        if( image != null )
        //        {
        //            g2.drawImage( image, null, ( int ) x - (image.getWidth() /2), ( int ) y - 100 );
        //        }

    }

    public List<Arrow> getArrows()
    {
        return arrows;
    }

    public void setArrows( int arrows )
    {
        this.numberOfArrows = arrows;
    }

    public void removeArrow( Arrow arrow )
    {
        this.arrows.remove( arrow );
    }

    public int getTeam()
    {
        return team;
    }

    public void setTeam( int team )
    {
        if( team == 0 )
        {
            return;
        }
        else
        {
            this.team = team;
        }
    }

    public void addArrow()
    {
        this.numberOfArrows++;
        Sound.playCollectArrow();
    }

    public int getNumberOfArrows()
    {
        return numberOfArrows;
    }

    public void addKill()
    {
        this.kills++;
        if( this.arenaModel.getTeam1().size() != 0 && this.arenaModel.getTeam2().size() != 0 )
        {

            final int[] team1Kills = { 0 };
            final int[] team2Kills = { 0 };

            this.arenaModel.getTeam1().forEach( player -> team1Kills[ 0 ] += player.getKills() );
            this.arenaModel.getTeam2().forEach( player -> team2Kills[ 0 ] += player.getKills() );
            System.out.printf( "team 1 has %s kills!\nteam 2 has %s kills!\n", team1Kills[ 0 ], team2Kills[ 0 ] );
        }
    }

    public int getKills()
    {
        return kills;
    }

    public int getPlayerNumber()
    {
        return playerNumber;
    }

    public void setPlayerNumber( int playerNumber )
    {
        this.playerNumber = playerNumber;
    }

    public void setDead()
    {
        this.dead = true;
    }

    public boolean isDead()
    {
        return dead;
    }

    public void removeKill()
    {
        this.kills--;
    }

    public void setAlive()
    {
        this.dead = false;
    }

    public int getStartX()
    {
        return startX;
    }

    public int getStartY()
    {
        return startY;
    }

    public void resetArrows()
    {
        this.arrows = new ArrayList<>();
    }

    @Override public int compareTo( Player o )
    {
        if( this.getKills() == o.getKills() )
        {
            return 0;
        }
        else if( this.getKills() > o.getKills() )
        {
            return -1;
        }
        else
        {
            return 1;
        }
    }

	public int getPlayerDatabaseId() {
		return playerDatabaseId;
	}

	public void setPlayerDatabaseId(int playerDatabaseId) {
		this.playerDatabaseId = playerDatabaseId;
	}
}