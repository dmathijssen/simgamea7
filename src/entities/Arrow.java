package entities;

import arena.ArenaModel;
import sounds.Sound;

import java.awt.*;
import java.awt.geom.Area;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by gjoosen on 29/05/15.
 */
public class Arrow extends Draw
{

    private final int GRAVITY_BASE = 30;
    private double velocity;
    private Point2D toPoint;
    private Player player;
    private double xMove, yMove;
    private boolean generalArrow = false;
    private double gravity = GRAVITY_BASE;
    private double delta;
    private boolean hit = false;


    public Arrow( int x, int y, BufferedImage bufferedImage, ArenaModel arenaModel, double velocity, Point2D toPoint, Player player )
    {
        super(x, y, bufferedImage, 0, 1, arenaModel);
        this.velocity = velocity;
        this.toPoint = toPoint;
        this.player = player;
        this.calculateMovement();

        delta = 0;
    }

    private void calculateMovement()
    {
        double xPointer = this.getToPoint().getX();
        double yPointer = this.getToPoint().getY();

        double xDelta = xPointer - super.x;
        double yDelta = yPointer - super.y;

        //rotation
        double rotation = Math.toDegrees( Math.atan2( yDelta, xDelta ) );
        super.setRotation( rotation );

        //movement
        yMove = Math.sin( Math.toRadians( rotation ) );
        xMove = Math.cos( Math.toRadians( rotation ) );
    }

    @Override
    public void drawCollision( Graphics2D g2 ){
        Shape shape = new Rectangle2D.Double( 0, 0, width, height );
        Area area = new Area( shape );
        area.transform( super.affineTransform() );
        g2.draw( area );
    }

    @Override
    public void updatePosition( double x, double y ){
        //check collision
        Shape shape = new Rectangle2D.Double( 0, 0, width, height );
        Area area = new Area( shape );
        area.transform( super.affineTransform() );

        final boolean collide[] = { false };
        final int[] xPos = { 0 };
        final int[] yPos = { 0 };
        this.arenaModel.getArenaController().getLevelController().getLevelModel().getCollisionMap().stream().forEach( e -> {
            e.stream().forEach( b -> {
                if( b )
                {
                    Rectangle2D rectangle2D = new Rectangle.Double( xPos[ 0 ] * 32, yPos[ 0 ] * 32, 32, 32 );
                    if( area.intersects( rectangle2D ) )
                    {
                        collide[ 0 ] = true;
                    }
                }
                xPos[ 0 ] += 1;
            } );
            xPos[ 0 ] = 0;
            yPos[ 0 ] += 1;
        } );

        //player collision
        List<Player> players = this.arenaModel.getPlayersMap().keySet().stream().collect( Collectors.toList() );

        if( velocity > 0 )
        {
            players.stream().forEach( e -> {
                if( e != this.player  && !e.isDead())
                {
                    Rectangle2D player = new Rectangle2D.Double( e.x, e.y, e.width, e.height );
                    if( area.intersects( player ) )
                    {
                        List<Arrow> killedPlayerArrows = e.getArrows();
                        killedPlayerArrows.forEach( arrow -> arrow.setGeneralArrow( true ) );
                        this.arenaModel.addArrowsToGeneralArrows( killedPlayerArrows );

                        //TODO stop remove
//                        this.arenaModel.getPlayersMap().remove(e);

                        e.setDead();
                        //give player a kill
                        if(this.player.getTeam() != -1 && this.player.getTeam() == e.getTeam()){
                            this.player.removeKill();
                        }else{
                            this.player.addKill();
                        }

                        this.player.removeArrow(this);
                        Sound.playPlayerHit();
                    }
                }
            } );
        }
        else
        {
            //collision check (no damage)
            players.stream().forEach( e -> {
                Rectangle2D player = new Rectangle2D.Double( e.x, e.y, e.width, e.height );
                if( area.intersects( player ) )
                {
                    //on collision, give a player an extra arrow.
                    e.addArrow();
                    this.player.removeArrow( this );
                    if( generalArrow )
                    {
                        this.arenaModel.getGeneralArrows().remove( this );
                    }
                }
            } );
        }

        if(!collide[0])
        {
            double newPosX = super.getX() + x;
            double newPosY = super.getY() + y + ( gravity * delta );
            double deltaX = newPosX - super.getX();
            double deltaY = newPosY - super.getY();
            super.setRotation( Math.toDegrees( Math.atan2( deltaY, deltaX )) );

            super.updatePosition( x, y + ( gravity * delta ) );
            gravity += 100 * delta;
        }
        else
        {
            gravity = GRAVITY_BASE;
            this.velocity = 0;
            //play sound when arrow hits floor
            if( !hit )
            {
                Sound.playFloorHit();
                hit = true;
            }
        }
    }

    public boolean borderCheck( int levelWidth, int levelHeight )
    {
        double arrowX = getX();
        double arrowY = getY();
        if( arrowX > levelWidth )
        {
            //arrow hits the right level border
            this.setX( 0 );
            //try to prevent collision bugs by adding pixels to the yValue of the player
            this.setY( arrowY - 10 );
            return false;
        }
        else if( arrowX < 0 )
        {
            //arrow hits the left level border
            this.setX( levelWidth - this.getWidth() );
            //try to prevent collision bugs by adding pixels to the yValue of the player
            this.setY( arrowY - 10 );
            return false;
        }
        else if(arrowY > levelHeight){
        	//arrow hits south level border
        	this.setY( 0 );
        	return false;
        }
        else
        {
            return true;
        }
    }

    public double getVelocity()
    {
        return velocity;
    }

    public Point2D getToPoint()
    {
        return toPoint;
    }

    public double getxMove()
    {
        return xMove;
    }

    public double getyMove()
    {
        return yMove;
    }

    public void setDelta( double delta )
    {
        this.delta = delta;
    }

    public void setGeneralArrow( boolean generalArrow )
    {
        this.generalArrow = generalArrow;
    }
}
