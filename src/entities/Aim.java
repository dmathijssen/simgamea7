package entities;

import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;

import arena.ArenaModel;

public class Aim extends Draw {

	public Aim(int x, int y, BufferedImage bufferedImage, double rotation,
		double scale, ArenaModel arenaModel) {
		super(x, y, bufferedImage, rotation, scale, arenaModel);		
	}

	public void setPosition(Point2D aimpoint) {
		setPosition(aimpoint.getX(),aimpoint.getY());
	}

}
