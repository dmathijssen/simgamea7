package entities;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.List;

import arena.ArenaModel;

/**
 * Created by gjoosen on 22/05/15.
 */
public abstract class Draw {

    protected double x, y;
    protected int width, height;
    protected BufferedImage bufferedImage;
    protected ArenaModel arenaModel;

    //affinetransform
    private double rotation, scale;

    public Draw(int x, int y, BufferedImage bufferedImage, double rotation, double scale, ArenaModel arenaModel){
        this.bufferedImage = bufferedImage;
        this.x = x;
        this.y = y;
        this.width = this.bufferedImage.getWidth();
        this.height = this.bufferedImage.getHeight();
        this.rotation = rotation;
        this.scale = scale;
        this.arenaModel =arenaModel;
    }

    protected AffineTransform affineTransform(){
        AffineTransform affineTransform = new AffineTransform();
        affineTransform.translate(this.x, this.y);
        affineTransform.scale(this.scale, this.scale);
        affineTransform.rotate(Math.toRadians(this.rotation), this.width/2, this.height/2);
        return affineTransform;
    }

    public void drawCollision(Graphics2D g2){
        g2.drawRect((int) x, (int) y, width, height);
    }

    /**
     * the draw method, draws the image with the affinetransform to the g2 object.
     * @param g2
     */
    public void draw(Graphics2D g2){
        g2.drawImage(this.bufferedImage, this.affineTransform(), null);
    }

    /**
     * add the x and y to the x and y position
     * @param x to add to the x
     * @param y to add to the y
     */
    public void updatePosition(double x, double y){
        this.x += x;
        this.y += y;
    }
    
    /**
     * Set the position of the opbect to a position
     * @param point of the object
     */
    public void setPosition(Point2D point){
    	setPosition(point.getX(), point.getY());
    }

    public void setBufferedImage(BufferedImage b)
    {
        this.bufferedImage = b;
    }
    
    /**
     * Set the position of the object to a position
     * @param x the x value of the object
     * @param y the y value of the object
     */
    public void setPosition(double x, double y){
    	this.x = x;
    	this.y = y;
    }

    /**
     * add the rotation to the rotation (new rotation = old rotation + rotation)
     * @param rotation
     */
    public void updateRotation(double rotation){
        this.rotation += rotation;
        if(this.rotation > 360){
            this.rotation -= 360;
        }
    }
    
    public Point2D getPosition(){
    	return new Point2D.Double(x,y);
    }
	public boolean isCollision(){
		boolean isCollision = false;
		List<Draw> objects = arenaModel.getCollissionObjects();
		for(Draw d : objects){
			if(d!=this){
				isCollision = (checkCollision(this.getPosition(),d.getPosition(), this.width,this.height ,d.getWidth(),d.getHeight()));
			}
		}
		return isCollision;
	}
	
	public static boolean checkCollision(Point2D point1, Point2D point2, int width1, int height1, int width2, int height2){
		//4 corners of the first object
		Rectangle rectangle1 = new Rectangle((int)point1.getX(), (int)point1.getY(), width1, height1);
		Rectangle rectangle2 = new Rectangle((int)point2.getX(), (int)point2.getY(), width2, height2);
		
		return rectangle1.intersects(rectangle2);
	}
	
	public int getHeight(){
		return height;
	}
	
	public int getWidth(){
		return width;
	}

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
    
    public void setX(double x){
    	this.x = x;
    }
    
    public void setY(double y){
    	this.y = y;
    }

    public void setRotation(double rotation){
        this.rotation = rotation;
    }



}
