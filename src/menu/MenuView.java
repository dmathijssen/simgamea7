package menu;

import game_options.OptionScreen;
import game_options.StartScreen;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JFrame;
import javax.swing.JPanel;

import arena.ArenaView;
import arena.Test;
import arena.WiiMoteController;

/**
 * Created by gjoosen on 21/05/15.
 */
@SuppressWarnings("serial")
public class MenuView extends JPanel implements KeyListener {

	
	private Dimension screensize = Toolkit.getDefaultToolkit().getScreenSize();
	public JFrame f;
	WiiMoteController wmc;
	private int players;
	private HashMap<Integer,Integer> playersDatabaseID = new HashMap<Integer, Integer>();
	
	private OptionScreen currentScreen;

	public MenuView() {
		currentScreen = new StartScreen(this);
		f = new JFrame("Menu");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setMaximumSize(screensize);
		f.setUndecorated(true);
		f.addKeyListener(this);

		f.setSize(800, 600);
		f.setVisible(true);
		f.add(currentScreen);
		f.setExtendedState(JFrame.MAXIMIZED_BOTH);
		
		addKeyListener(this);
		repaint();
	}

	public void addNotify() {
		super.addNotify();
		requestFocus();
	}


	public void keyPressed(KeyEvent e) {
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		switch (arg0.getKeyCode()) {
		case KeyEvent.VK_ENTER:
			currentScreen.select();
			break;
		case KeyEvent.VK_RIGHT:
			currentScreen.moveRightSelection();
			break;
		case KeyEvent.VK_LEFT:
			currentScreen.moveLeftSelection();
			break;
		

		default:
			break;
		}
		currentScreen.keyPressed(arg0);
	}

	public void launchGame(ArrayList<Integer> teams) {
		ArenaView arenaView = null;
		f.remove(currentScreen);
		if (teams == null) {
			arenaView = Test.launchGame(players, f , wmc, playersDatabaseID);
			f.add(arenaView);
		} else {
			arenaView = Test.launchGameWithTeams(teams, f, wmc, playersDatabaseID);
			f.add(arenaView);
		}
		f.revalidate();
		arenaView.revalidate();
	}
	
	public void setWmc(WiiMoteController wmc){
		this.wmc = wmc;
	}
	
	public void setPanel(OptionScreen screen){
		f.remove(currentScreen);
		this.currentScreen = screen;
		f.add(screen);
		screen.revalidate();
		f.revalidate();
	}
	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	public void setPlayers(int players){
		this.players = players;
	}
	
	public int getPlayers(){
		return players;
	}
	
	public void setPlayerCollection(HashMap<Integer,Integer> playerCollection){
		this.playersDatabaseID = playerCollection;
	}
}
