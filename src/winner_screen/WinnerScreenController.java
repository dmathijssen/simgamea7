package winner_screen;

import java.util.Collections;
import java.util.List;
import db.Database;
import entities.Player;

/**
 * Created by gjoosen on 05/06/15.
 */
public class WinnerScreenController {

	private WinnerScreenView winnerScreenView;

	private boolean team;
	private List<Player> team1;
	private List<Player> team2;
	private List<Player> players;
	Database db = new Database();

	private int team1Score = 0, team2Score = 0;

	public WinnerScreenController(List<Player> team1, List<Player> team2) {
		this.team1 = team1;
		this.team2 = team2;
		this.team = true;
		this.winnerScreenView = new WinnerScreenView(this);

		// save points to db and register kills for end screen
		for (Player p : team1) {
			//player gets 10 points for each kill they made
			int kills = p.getKills();
			team1Score += kills;
			try {
				db.savePoints(p.getPlayerDatabaseId(), kills * 10);
				} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		
		for (Player p : team2) {
			int kills = p.getKills();
			team2Score += kills;
			//player gets 10 points for each kill they made
			try {
				db.savePoints(p.getPlayerDatabaseId(), kills * 10);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}

	public WinnerScreenController(List<Player> players) {
		this.players = players;
		this.team = false;
		this.winnerScreenView = new WinnerScreenView(this);
		
		//save points to database
		for (Player p : players) {
			//player gets 10 points for each kill they made
			try {
				db.savePoints(p.getPlayerDatabaseId(), p.getKills() * 10);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}

		// free for all
		Collections.sort(this.getPlayers());
		this.getPlayers().forEach(
				e -> System.out.println((e.getPlayerNumber() + 1) + " - "
						+ e.getKills()));
	}

	public WinnerScreenView getWinnerScreenView() {
		return winnerScreenView;
	}

	public boolean getTeam() {
		return team;
	}

	public List<Player> getTeam1() {
		return team1;
	}

	public List<Player> getTeam2() {
		return team2;
	}

	public List<Player> getPlayers() {
		return players;
	}

	public boolean isTeam() {
		return team;
	}

	public int getTeam1Score() {
		return team1Score;
	}

	public int getTeam2Score() {
		return team2Score;
	}
}
