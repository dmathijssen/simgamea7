package winner_screen;

import javax.imageio.ImageIO;
import javax.swing.*;

import entities.Player;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

/**
 * Created by gjoosen on 05/06/15.
 */
public class WinnerScreenView extends JPanel
{

	private BufferedImage leaderboard;
    private WinnerScreenController controller;
    public WinnerScreenView( WinnerScreenController controller )
    {
        this.controller = controller;
        loadImage();
    }
    public void loadImage()
    {
    	try {
			leaderboard = ImageIO.read(new File("res/selection/leaderbord.png"));
        } catch (Exception e) {
			// TODO: handle exception
		}
    }
    @Override protected void paintComponent( Graphics g )
    {
        Graphics2D g2 = ( Graphics2D ) g;
        g2.drawImage(leaderboard, null, 0,0);
        g2.setColor(Color.BLACK);
        if( this.controller.isTeam() )
        {
        	Font font = new Font("Lucida",Font.ITALIC,32);
        	g2.setFont(font);

        	//draw headers
            g2.drawString("Team",571,524);
            g2.drawString("Kills",729,524);
            g2.drawString("Points",887,524);
            g2.drawLine(550,550,1000,550);
        	
        	//team 1 won
            if( this.controller.getTeam1Score() > this.controller.getTeam2Score() )
            {
            	
                //draw player points
                int yPoints = 1;
                for(Player player : controller.getTeam1()){                	
                	g2.drawString("Player "+ (player.getPlayerNumber()+1), 591, 600 + yPoints * 30);
                	g2.drawString(" + " + player.getKills() * 10 + "points", 845, 600 + yPoints * 30);
                	yPoints++;
                }
                yPoints = 1;
                for(Player player : controller.getTeam2()){
                	g2.drawString("Player "+ (player.getPlayerNumber()+1), 591, 700 + yPoints * 30);
                	g2.drawString(" + " + player.getKills() * 10 + "points", 845, 700 + yPoints * 30);
                	yPoints++;
                }
                //draw team names and scores
                g2.setColor(Color.red);
                g2.drawString( "Team 1: ",571,600);
                g2.drawString("Team",1163,754);
                g2.drawString("1",1189,782);
                g2.setColor(Color.black);
                g2.drawString( "" + this.controller.getTeam1Score(),739,600);
                g2.setColor(Color.blue);
                g2.drawString( "Team 2: ",571,700);
                g2.setColor(Color.black);
                g2.drawString( "" + this.controller.getTeam2Score(),739,700);
            }
            //team 2 won
            else
            {
                //draw player points
                int yPoints = 1;
                for(Player player : controller.getTeam2()){
                	g2.drawString("Player "+ (player.getPlayerNumber()+1), 591, 600 + yPoints * 30);
                	g2.drawString(" + " + player.getKills() * 10 + "points", 845, 600 + yPoints * 30);
                	yPoints++;
                }
                yPoints = 1;
                for(Player player : controller.getTeam1()){
                	g2.drawString("Player "+ (player.getPlayerNumber()+1), 591, 700 + yPoints * 30);
                	g2.drawString(" + " + player.getKills() * 10 + "points", 845, 700 + yPoints * 30);
                	yPoints++;
                }
                //draw team names and scores
                g2.setColor(Color.blue);
                g2.drawString( "Team 2: ",571,600);
                g2.drawString("Team",1163,754);
                g2.drawString("2",1189,782);
                g2.setColor(Color.black);
                g2.drawString( "" + this.controller.getTeam2Score(),739,600);
                g2.setColor(Color.red);
                g2.drawString( "Team 1: ",571,700);
                g2.setColor(Color.black);
                g2.drawString( "" + this.controller.getTeam1Score(),739,700);
            }


        }
        else
        {
            //free for all
            final int[] y = { 0 };
            //set font and draw headers
        	Font font = new Font("Lucida",Font.ITALIC,32);
        	g2.setFont(font);
        	g2.setColor(Color.BLACK);
        	g2.drawString("Players", 571,524);
            g2.drawString("Kills",729,524);
            g2.drawString("Points",839,524);
            g2.drawLine(550,550,1000,550);
            this.controller.getPlayers().forEach( player -> {
            	int kills = player.getKills();
                g2.drawString( "Player " + ( player.getPlayerNumber() + 1 ),571,592+ y[ 0 ] * 30 );
                g2.drawString( "" + kills,739,592 + y[ 0 ] * 30 );
                g2.drawString("+ " + kills * 10, 850, 592 + y[ 0 ] * 30 );
                y[ 0 ]++;
            } );
        }
    }
}
