package options;

import game_options.OptionScreen;
import game_options.StartScreen;
import menu.MenuView;
import sounds.Sound;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.*;

/**
 * Created by gjoosen on 11/06/15.
 */
public class SoundOptions extends OptionScreen{

    public static boolean musicOn = true, soundEffectOn = true;

    protected BufferedImage background;

    private int option = 0;
    private String[] options = {"Music", "Effects", "Back"};

    public SoundOptions(MenuView view, int amountPositions) {
        super(view, amountPositions);
        try {
            background = ImageIO.read(new File("res/selection/jungla.png"));
        }catch (IOException ex){
            ex.printStackTrace();
        }
    }

    @Override
    public void select() {
        //enter
        switch (option){
            case 0:
                musicOn = !musicOn;
                if(!musicOn){
                    Sound.stopGameTheme();
                }else{
                    Sound.playGameTheme();
                }
                break;
            case 1:
                soundEffectOn = !soundEffectOn;
                break;
            case 2:
                //back
                super.view.setPanel(new StartScreen(super.view));
                break;
        }
        writeSettings();
        repaint();
    }

    @Override
    public void keyPressed(KeyEvent e) {
        //pijltjes
        switch (e.getKeyCode()){
            case KeyEvent.VK_DOWN:
                option++;
                break;
            case KeyEvent.VK_UP:
                option--;
                break;
        }
        if(option == this.options.length){
            option = 0;
        }else if(option == -1){
            option = options.length-1;
        }
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.drawImage(background, null, 0, 0);

        g2d.setColor(Color.RED);
        for(int i = 0; i < options.length; i++){
            if(option == i){
                g2d.setColor(Color.YELLOW);
            }

            switch(i){
                case 0:
                    if(musicOn){
                        g2d.drawString(options[i] + " on", 10, 20 * i + 10);
                    }else{
                        g2d.drawString(options[i] + " off", 10, 20 * i + 10);
                    }
                    break;
                case 1:
                    if(soundEffectOn){
                        g2d.drawString(options[i] + " on", 10, 20 * i + 10);
                    }else{
                        g2d.drawString(options[i] + " off", 10, 20 * i + 10);
                    }
                    break;
                case 2:
                    g2d.drawString(options[i], 10, 20 * i + 10);
                    break;
            }
            g2d.setColor(Color.RED);
        }
    }

    private static void writeSettings(){
        try(FileWriter outputStream = new FileWriter("./res/options.txt")){
            outputStream.write("music:" + musicOn);
            outputStream.write("\nsoundeffects:" + soundEffectOn);
            outputStream.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void readSettings(){
        try(BufferedReader reader = new BufferedReader(new FileReader("./res/options.txt"))){
            String music = reader.readLine();
            String effects = reader.readLine();

            if(music.split(":")[1].equals("true")){
                musicOn = true;
            }else{
                musicOn = false;
            }

            if(effects.split(":")[1].equals("true")){
                soundEffectOn = true;
            }else{
                soundEffectOn = false;
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(!musicOn){
            Sound.stopGameTheme();
        }
    }
}
