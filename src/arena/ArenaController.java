package arena;

import entities.Aim;
import entities.Arrow;
import entities.Draw;
import entities.Player;
import level.LevelController;
import winner_screen.WinnerScreenController;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;

/**
 * Created by gjoosen on 21/05/15.
 */
public class ArenaController
{

    private static final int TEAM_WINNER_SCORE = 2;
    private static final int FREE_FOR_ALL = 4;

    private static final int FPS = 60;
    // updater
    long lastStartTime = System.currentTimeMillis();
    // arena
    private ArenaModel arenaModel;
    private ArenaView arenaView;
    private JFrame frame;
    private BufferedImage leaderboard;
    // level
    private LevelController levelController;

    private Timer repaintTimer, updateTimer;

    public ArenaController( ArenaModel model, ArenaView view, JFrame frame)
    {
        this.arenaModel = model;
        this.arenaView = view;
        this.frame = frame;

        this.arenaModel.setController( this );
        this.arenaView.setController( this );

        // level
        this.levelController = new LevelController();


        // repaint timer
        this.repaintTimer = new Timer( 1000 / FPS, e -> {
            this.arenaView.repaint();
        } );
        repaintTimer.start();

        // update timer
        this.updateTimer = new Timer( 1, e -> {
            this.gameUpdate();
        } );
        updateTimer.start();
        loadImage();
    }

    public void startGame()
    {
        boolean wiimotes = true;
        if( wiimotes )
        {
            if(!System.getProperty("os.name").equals("Mac OS X")){
                try
                {
                    new WiiMoteController( 4, this );
                }
                catch( NoControllersFoundException e )
                {
                    e.printStackTrace();
                }
            }
        }
    }

    private void gameUpdate()
    {
        long start = System.currentTimeMillis();
        double delta = ( start - lastStartTime ) / 1000.0;
        int levelWidth = levelController.getLevelWidth();
        int levelHeight = levelController.getLevelHeight();

//        System.out.println(delta);

        //shallow copy to prevent concurrentmodified exception
        Map<Player, Aim> players = new HashMap<>( arenaModel.getPlayersMap() );
        players.keySet().forEach( p -> {
            double playerX = p.getX();
            double playerY = p.getY();
            int playerWidth = p.getWidth();
            //player hits the right level border
            if( playerX + playerWidth / 2 > levelWidth )
            {
                p.setX( 0 );
                //try to prevent collision bugs by adding pixels to the yValue of the player
                p.setY( playerY - 1 );
            }
            //player hits the left level border
            if( playerX + playerWidth / 2 < 0 )
            {
                p.setX( levelWidth - p.getWidth() );
                //try to prevent collision bugs by adding pixels to the yValue of the player
                p.setY( playerY - 1 );
            }
            //player falls through bottom of level
            if(playerY > levelHeight){
            	p.setY(0);
            }
            p.move( delta );
            p.updateArrows( delta );
        } );

        //update general arrows
        ConcurrentLinkedQueue<Arrow> arrows = new ConcurrentLinkedQueue<>( this.arenaModel.getGeneralArrows() );
        arrows.forEach( arrow -> {
            arrow.setDelta( delta );
            //if borderCheck passes, update arrow movement
            if( arrow.borderCheck( levelWidth, levelHeight ) )
            {
                arrow.updatePosition( arrow.getxMove() * delta * arrow.getVelocity(), arrow.getyMove() * delta * arrow.getVelocity() );
            }
        } );

        //check for team scores
        if(this.getArenaModel().getTeam1().size() != 0 && this.arenaModel.getTeam2().size() != 0){
            if(this.getArenaModel().getTeam1().stream().filter(e -> !e.isDead()).collect(Collectors.toList()).size() == 0 || this.getArenaModel().getTeam2().stream().filter(e -> !e.isDead()).collect(Collectors.toList()).size() == 0){
                //rest teams
                if(this.getArenaModel().getTeamScores()[0] == TEAM_WINNER_SCORE){
                    System.out.println("Team 1 wins!");
                    this.setwinnerScreen();
                }else if(this.getArenaModel().getTeamScores()[1] == TEAM_WINNER_SCORE){
                    System.out.println("Team 2 wins!");
                    this.setwinnerScreen();
                }else{
                    this.resetPlayers();
                }
            }

            //check winner
            if(this.getArenaModel().getTeamScores()[0] == TEAM_WINNER_SCORE){
                System.out.println("Team 1 wins!");
                this.setwinnerScreen();
            }else if(this.getArenaModel().getTeamScores()[1] == TEAM_WINNER_SCORE){
                System.out.println("Team 2 wins!");
                this.setwinnerScreen();
            }
        }else{
            //check winner
            this.arenaModel.getPlayersMap().keySet().forEach(player -> {
                if(player.getKills() == FREE_FOR_ALL){
                    System.out.printf("Player %s wins!", player.getPlayerNumber());
                    this.setwinnerScreen();
                }else{
                    if(this.getArenaModel().getPlayersMap().keySet().stream().filter(e->!e.isDead()).collect(Collectors.toList()).size() == 1){
                        //rest free for all
                        System.out.println("reset");
                        this.resetPlayers();
                    }
                }
            });
        }

        // game logic
        Toolkit.getDefaultToolkit().sync();
        lastStartTime = start;
    }
    
    public void loadImage()
    {
    	try {
			leaderboard = ImageIO.read(new File("res/selection/leaderbord2.png"));
		} catch (Exception e) {
			// TODO: handle exception
		}
    }

    private void resetPlayers(){
        //reset players position + set alive
        this.getArenaModel().getPlayersMap().keySet().stream().forEach(player -> {
            player.setAlive();
            player.setPosition(player.getStartX(), player.getStartY());
            player.setArrows(4);
            player.resetArrows();
        });

        //remove arrows
        this.getArenaModel().resetGeneralArrows();

        //TODO malek design
        Graphics2D g2 = (Graphics2D) this.arenaView.getGraphics();
        g2.drawImage(leaderboard, null, 0, 0);
        
        g2.setColor(Color.DARK_GRAY);

        //team
        if(this.getArenaModel().getTeam1().size() != 0 && this.getArenaModel().getTeam2().size() != 0){
            //team
        	Font font = new Font("Lucida",Font.ITALIC,128);
        	g2.setFont(font);
            if(this.getArenaModel().getTeam1().size() != 0){
                g2.drawString("team 1",748,620);
                g2.drawString("WON",768,720);
            }else{
                g2.drawString("team 2",748,620);
                g2.drawString("WON",768,720);
            }
        }else{
            //no team
        	Font font = new Font("Lucida",Font.ITALIC,32);
            List<Player> players = this.getArenaModel().getPlayersMap().keySet().stream().filter(e->!e.isDead()).collect(Collectors.toList());
            final int[] y = { 0 };
            g2.setFont(font);
            g2.drawString("Players", 571,524);
            g2.drawString("Kills",729,524);
            g2.drawLine(550,550,1375,550);
            players.forEach(player ->{
            	g2.drawString("Player " + (player.getPlayerNumber() + 1), 571,592 +  y[ 0 ] * 30 );
            	g2.drawString("" + player.getKills(),739,592 +  y[ 0 ] * 30 );
                y[ 0 ]++;
            });
        }


        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    private void setwinnerScreen(){

        //stop the timers
        this.repaintTimer.stop();
        this.updateTimer.stop();

        WinnerScreenController winnerScreenController;

        if(this.getArenaModel().getTeam1().size() != 0 && this.arenaModel.getTeam2().size() != 0){
            winnerScreenController = new WinnerScreenController(this.arenaModel.getTeam1(), this.arenaModel.getTeam2());
        }else{
            winnerScreenController = new WinnerScreenController(this.arenaModel.getPlayersMap().keySet().stream().collect(Collectors.toList()));
        }

        this.frame.remove(this.arenaView);
        this.frame.add(winnerScreenController.getWinnerScreenView());
        this.frame.repaint();
        this.frame.revalidate();
    }

    public List<Draw> getShapes()
    {
        return this.arenaModel.getDrawObjects();
    }

    public void keyPressed( char e, int playerNumber )
    {
        arenaModel.movePlayer( e, playerNumber );
    }

    public void keyReleased( char e, int playerNumber )
    {
        arenaModel.resetPlayerMovement( e, playerNumber );
    }

    public void aimPlayer( Point2D aimpoint, int playerNumber )
    {
        arenaModel.aimPlayer( aimpoint, playerNumber );
    }

    public ArenaView getArenaView()
    {
        return arenaView;
    }

    public void playerAimed( Point2D aimpoint, int playerNumber )
    {
        arenaModel.aimPlayer( aimpoint, playerNumber );
    }

    public LevelController getLevelController()
    {
        return levelController;
    }

    public ArenaModel getArenaModel()
    {
        return this.arenaModel;
    }

    public void initPlayerFaces()
    {
        final int[] i = { 0 };
        arenaModel.getPlayersMap().keySet().forEach( p -> {

            try
            {
                arenaModel.getPlayerfaces().add( ImageIO.read( new File( "res/webcampictures/player" + i[ 0 ] + ".png" ) ) );
            }
            catch( IOException e )
            {
            }
            i[ 0 ]++;
        } );
    }
}
