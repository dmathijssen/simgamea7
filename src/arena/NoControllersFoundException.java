package arena;

@SuppressWarnings("serial")
public class NoControllersFoundException extends Exception
{
    public NoControllersFoundException( String string )
    {
        super( string );
    }

    public NoControllersFoundException()
    {
        super( "No controllers have been found." );
    }

}
