package arena;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by imegumii on 6/11/15.
 */
public class SpriteGetter
{
    int player;

    BufferedImage restRightSprite;
    BufferedImage restLeftSprite;
    BufferedImage walkRightSprite;
    BufferedImage walkLeftSprite;
    BufferedImage jumpRightSprite;
    BufferedImage jumpLeftSprite;

    int xWalkRight, xWalkLeft, xJumpRight, xJumpLeft;
    int countXWalkRight, countXWalkLeft;


    public SpriteGetter(int player)
    {
        this.player = player;
        xWalkLeft = 0;
        xWalkRight = 0;
        xJumpLeft = 0;
        xJumpRight = 0;

        countXWalkLeft = 0;
        countXWalkRight = 0;

        try
        {
            loadImages();
        }
        catch( IOException e )
        {
            e.printStackTrace();
        }
    }

    public void loadImages() throws IOException
    {
        restRightSprite = ImageIO.read(new File("res/player" + player + "/rest-rechts.png"));
        restLeftSprite = ImageIO.read(new File("res/player" + player + "/rest-links.png"));
        walkRightSprite = ImageIO.read(new File("res/player" + player + "/lopen-rechts.png"));
        walkLeftSprite = ImageIO.read(new File("res/player" + player + "/lopen-links.png"));
        jumpRightSprite = ImageIO.read(new File("res/player" + player + "/jump-rechts.png"));
        jumpLeftSprite = ImageIO.read(new File("res/player" + player + "/jump-links.png"));
    }

    public BufferedImage getRestRightSprite()
    {
        return restRightSprite;
    }

    public BufferedImage getRestLeftSprite()
    {
        return restLeftSprite;
    }

    public BufferedImage getWalkingRightSprite()
    {
        if( (countXWalkRight % 50) == 0 )
        {
            xWalkRight += walkRightSprite.getWidth() / 3;
            countXWalkRight = 0;
            if( xWalkRight >= walkRightSprite.getWidth() - (walkRightSprite.getWidth() / 3) )
            {
                xWalkRight = 0;
                xWalkLeft = 0;
            }
        }
        countXWalkRight++;
        return walkRightSprite.getSubimage(xWalkRight, 0, walkRightSprite.getWidth() / 3, walkRightSprite.getHeight());
    }

    public BufferedImage getWalkingLeftSprite()
    {
        if( (countXWalkLeft % 50) == 0 )
        {
            xWalkLeft += walkLeftSprite.getWidth() / 3;
            countXWalkLeft = 0;
            if( xWalkLeft >= walkLeftSprite.getWidth() - (walkLeftSprite.getWidth() / 3) )
            {
                xWalkRight = 0;
                xWalkLeft = 0;
            }
        }
        return walkLeftSprite.getSubimage(xWalkLeft, 0, walkLeftSprite.getWidth() / 3, walkLeftSprite.getHeight());
    }

    public BufferedImage getJumpingRightSprite(boolean jumping)
    {
        if( jumping )
        {
            return jumpRightSprite.getSubimage(0, 0, jumpRightSprite.getWidth() / 2, jumpRightSprite.getHeight());
        }
        else
        {
            return jumpRightSprite.getSubimage(jumpRightSprite.getWidth() / 2, 0, jumpRightSprite.getWidth() / 2, jumpRightSprite.getHeight());
        }

    }

    public BufferedImage getJumpingLeftSprite(boolean jumping)
    {
        if( jumping )
        {
            return jumpLeftSprite.getSubimage(jumpLeftSprite.getWidth() / 2, 0, jumpLeftSprite.getWidth() / 2, jumpLeftSprite.getHeight());
        }
        else
        {
            return jumpLeftSprite.getSubimage(0, 0, jumpLeftSprite.getWidth() / 2, jumpLeftSprite.getHeight());
        }
    }
}
