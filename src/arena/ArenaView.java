package arena;

import entities.Player;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

/**
 * Created by gjoosen on 21/05/15.
 */

@SuppressWarnings( "serial" ) public class ArenaView extends JPanel implements KeyListener
{

    private ArenaController arenaController;
    private int player = 0;

    public ArenaView()
    {
        addKeyListener( this );
    }

    public void setController( ArenaController controller )
    {
        arenaController = controller;
    }

    @Override protected void paintComponent( Graphics g )
    {

        super.paintComponent( g );
        Graphics2D g2 = ( Graphics2D ) g;
        //mouse
            arenaController.aimPlayer(MouseInfo.getPointerInfo().getLocation(),0);
        

        if( arenaController.getShapes() != null )
        {
            if( this.arenaController.getLevelController() != null )
            {
                this.arenaController.getLevelController().draw( g2 );
            }
            if( arenaController.getShapes() != null )
            {
                arenaController.getShapes().forEach( e -> e.draw( g2 ) );
            }
        }

        boolean collision = true;
        if( collision )
        {
            g2.setColor( Color.pink );
            //		arenaController.getShapes().forEach(e-> e.drawCollision(g2));
            if( arenaController.getLevelController() != null )
            {
                arenaController.getLevelController().drawCollisionMap( g2 );
            }
            arenaController.getArenaModel().getDrawObjects().forEach( e -> e.drawCollision( g2 ) );
            arenaController.getArenaModel().getPlayersMap().keySet().forEach( player -> player.getArrows().forEach( arrow -> arrow.drawCollision( g2 ) ) );
        }

        //hud
        //        this.arenaController.getHudController().getHudView().draw(g2);

        //draw score
        if( this.arenaController.getArenaModel().getTeam1().size() != 0 && this.arenaController.getArenaModel().getTeam2().size() != 0 )
        {
            g2.drawString( "Team 1: " + this.arenaController.getArenaModel().getTeamScores()[ 0 ], 20, 20 );
            g2.drawString("Team 2: " + this.arenaController.getArenaModel().getTeamScores()[1], 20, 40);
        }
        else
        {
            this.arenaController.getArenaModel().getPlayersMap().keySet().forEach(player -> {
                g2.drawString("Player " + (player.getPlayerNumber() + 1) + ":" + player.getKills(), 20, 20 + player.getPlayerNumber() * 20);
            });
        }

        //draw facez
        int size = arenaController.getArenaModel().getPlayerfaces().size();
        ArrayList<Player> players = new ArrayList<Player>( arenaController.getArenaModel().getPlayersMap().keySet() );
        final int[] xval = { getWidth() - ( 50 + size * 150 ) };
        int yval = 30;
        final int[] i = { 1 };
        this.arenaController.getArenaModel().getPlayerfaces().forEach( b -> {
            g2.drawString( ( "Player " + i[ 0 ] ), xval[ 0 ], yval );
            g2.drawImage( b, null, xval[ 0 ], yval );
            if( players.get( i[ 0 ] - 1 ).isDead() )
            {
                g2.setColor( Color.red );
                g2.setStroke( new BasicStroke( 5, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND ) );
                g2.drawLine( xval[ 0 ], yval, xval[ 0 ] + b.getWidth(), yval + b.getHeight() );
                g2.drawLine( xval[ 0 ], yval + b.getHeight(), xval[ 0 ] + b.getWidth(), yval );
                g2.setColor( Color.pink );
            }


            xval[ 0 ] += 160;
            i[ 0 ]++;
        } );

    }

    public void addNotify()
    {
        super.addNotify();
        requestFocus();
    }

    @Override public void keyTyped( KeyEvent e )
    {
        if( e.getKeyChar() == 'n' )
        {
            arenaController.keyPressed( e.getKeyChar(), player );
            player++;
            if( player >= this.arenaController.getArenaModel().getPlayersMap().size() )
            {
                player = 0;
            }
        }
        else
        {
            if( player >= this.arenaController.getArenaModel().getPlayersMap().size() )
            {
                player = 0;
            }
            arenaController.keyPressed( e.getKeyChar(), player );
        }
        repaint();
    }

    @Override public void keyPressed( KeyEvent e )
    {
        // TODO Auto-generated method stub
    }

    @Override public void keyReleased( KeyEvent e )
    {
        // TODO Auto-generated method stub

        arenaController.keyReleased( e.getKeyChar(), 0 );
        repaint();
    }


}