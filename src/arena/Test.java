package arena;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamResolution;
import menu.MenuView;
import sounds.Sound;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class Test
{

    public static void main(String[] args)
    {
        Sound.playGameTheme();
        new MenuView();
    }

    public static void test()
    {
        Webcam webcam = Webcam.getDefault();
        webcam.setViewSize(WebcamResolution.VGA.getSize());
        webcam.open();

        // get image
        BufferedImage image = webcam.getImage();

        // save image to PNG file
        try
        {
            ImageIO.write(image, "PNG", new File("test.png"));
        }
        catch( IOException e )
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        webcam.close();
    }

    public static ArenaView launchGame(int players, JFrame frame, WiiMoteController wmc, HashMap<Integer, Integer> databaseIDs)
    {
        ArenaModel model = new ArenaModel();
        ArenaView view = new ArenaView();
        ArenaController controller = new ArenaController(model, view, frame);
        for( int i = 1; i <= players; i++ )
        {
            model.addPlayer(-1, databaseIDs.get(i - 1));
        }
        controller.initPlayerFaces();
        if( !System.getProperty("os.name").equals("Mac OS X") )
        {
            wmc.setController(controller);
        }
        return view;
    }

    /**
     * This method let the game start with two teams
     *
     * @param teams
     */
    public static ArenaView launchGameWithTeams(List<Integer> teams, JFrame frame, WiiMoteController wmc, HashMap<Integer, Integer> databaseIDs)
    {
        ArenaModel model = new ArenaModel();
        ArenaView view = new ArenaView();
        ArenaController controller = new ArenaController(model, view, frame);
        int j = 0;
        for( int i : teams )
        {
            System.out.println("id: " + j);
            model.addPlayer(i, databaseIDs.get(j));
            j++;
        }
        controller.initPlayerFaces();
        if(!System.getProperty("os.name").equals("Mac OS X")){
            wmc.setController(controller);
        }
        return view;
    }

}
