package arena;

import wiiusej.WiiUseApiManager;
import wiiusej.Wiimote;
import wiiusej.wiiusejevents.physicalevents.*;
import wiiusej.wiiusejevents.utils.WiimoteListener;
import wiiusej.wiiusejevents.wiiuseapievents.*;

import java.awt.*;
import java.awt.geom.Point2D;
import java.util.HashMap;

public class WiiMoteController implements WiimoteListener
{

    private HashMap<Integer, Integer> players;
    private ArenaController controller;

    public WiiMoteController( int players, ArenaController controller ) throws NoControllersFoundException
    {
        Wiimote[] wiimotes = WiiUseApiManager.getWiimotes( players, true );
        this.players = new HashMap<Integer, Integer>();
        this.controller = controller;

        // Screen size for virtual resolution
        //        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension d = new Dimension( 1920, 1080 );
        if( wiimotes != null )
        {
            for( int i = 0; i < wiimotes.length; i++ )
            {
                Wiimote wiimote = wiimotes[ i ];

                // Add wiimote to players
                this.players.put( wiimote.getId(), i );

                // Assign correct LED
                switch( i )
                {
                    case 0:
                        wiimote.setLeds( true, false, false, false );
                        break;
                    case 1:
                        wiimote.setLeds( false, true, false, false );
                        break;
                    case 2:
                        wiimote.setLeds( false, false, true, false );
                        break;
                    case 3:
                        wiimote.setLeds( false, false, false, true );
                        break;
                }

                // Vibrate once to indicate made connection
                wiimote.activateRumble();
                wiimote.deactivateRumble();

                wiimote.addWiiMoteEventListeners( this );

                // Activate IR tracking necessary for aiming
                wiimote.activateIRTRacking();

                // Asume 16:9 aspect ratio is used and sensor bar is below
                // screen
                wiimote.setScreenAspectRatio169();
                //                wiimote.setSensorBarBelowScreen();
                wiimote.setSensorBarAboveScreen();
                // Set virtual resolution to screen res
                wiimote.setVirtualResolution( ( int ) d.getWidth(), ( int ) d.getHeight() );
            }
        }
        else
        {
            throw new NoControllersFoundException( "No controllers found" );
        }
    }

    public void setController( ArenaController controller )
    {
        this.controller = controller;
    }

    @Override public void onButtonsEvent( WiimoteButtonsEvent e )
    {
        int player = players.get( e.getWiimoteId() );
        //Fire once
        if( e.isButtonAJustPressed() )
        {
            if( controller != null )
            {
                controller.keyPressed( 'q', player );
            }
        }
    }

    @Override public void onIrEvent( IREvent e )
    {
        Point2D aimpoint = new Point2D.Double( e.getX(), e.getY() );
        int player = players.get( e.getWiimoteId() );
        if( controller != null )
        {
            controller.aimPlayer( aimpoint, player );
        }
    }

    @Override public void onMotionSensingEvent( MotionSensingEvent arg0 )
    {

    }

    @Override public void onExpansionEvent( ExpansionEvent e )
    {
        NunchukEvent ne = ( NunchukEvent ) e;
        float magnitude = ne.getNunchukJoystickEvent().getMagnitude();
        int player = players.get( ne.getWiimoteId() );
        if( controller != null )
        {
            // If joystick is being moved around
            if( magnitude >= 0.5 )
            {
                // Movement controls go here
                int angle = ( int ) ne.getNunchukJoystickEvent().getAngle();
                if( angle < 135 && angle > 45 )
                {
                    // Player goes right
                    // System.out.println("right" + player);
                    controller.keyPressed( 'd', player );
                }
                else if( angle < 225 && angle > 135 )
                {
                    // Player goes down
                    // System.out.println("down" + player);
                    controller.keyPressed( 's', player );
                }
                else if( angle < 315 && angle > 225 )
                {
                    // Player goes left
                    // System.out.println("left" + player);
                    controller.keyPressed( 'a', player );

                }
            }
            else
            {
                controller.keyPressed( 'e', player );
            }
        }

        NunchukButtonsEvent be = ne.getButtonsEvent();
        // This continuously fires events
        if( be.isButtonCHeld() )
        {
            // Dash
            // System.out.println("button c is pressed");
            if( controller != null )
            {
                controller.keyPressed( 'c', player );
            }
        }


        // This continuously fires events
        if( be.isButtonZeHeld() )
        {
            // Jump
            // System.out.println("button z is pressed");
            if( controller != null )
            {
                controller.keyPressed( 'w', player );
            }
        }
        else //reset when not pressed, aka regulate jump height.
        {
            if( controller != null )
            {
                controller.keyReleased( 'w', player );
            }
        }

    }

    @Override public void onStatusEvent( StatusEvent arg0 )
    {

    }

    @Override public void onDisconnectionEvent( DisconnectionEvent arg0 )
    {

    }

    @Override public void onNunchukInsertedEvent( NunchukInsertedEvent arg0 )
    {

    }

    @Override public void onNunchukRemovedEvent( NunchukRemovedEvent arg0 )
    {

    }

    @Override public void onGuitarHeroInsertedEvent( GuitarHeroInsertedEvent arg0 )
    {

    }

    @Override public void onGuitarHeroRemovedEvent( GuitarHeroRemovedEvent arg0 )
    {

    }

    @Override public void onClassicControllerInsertedEvent( ClassicControllerInsertedEvent arg0 )
    {

    }

    @Override public void onClassicControllerRemovedEvent( ClassicControllerRemovedEvent arg0 )
    {

    }
}
