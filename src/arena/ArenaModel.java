package arena;

import entities.Aim;
import entities.Arrow;
import entities.Draw;
import entities.Player;

import javax.imageio.ImageIO;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by gjoosen on 21/05/15.
 */
public class ArenaModel
{

    private ArenaController arenaController;
    private List<Draw> drawObjects = new ArrayList<>();
    private Map<Player, Aim> players;
    private List<Arrow> generalArrows = new ArrayList<>();
    private List<Point2D> spawnPoints = new ArrayList<>();
    private List<Player> team1, team2;
    private ArrayList<BufferedImage> playerfaces = new ArrayList<>();


    public ArenaModel()
    {
        spawnPoints.add(new Point2D.Double(50, 500));
        spawnPoints.add(new Point2D.Double(250, 50));
        spawnPoints.add(new Point2D.Double(1100, 525));
        spawnPoints.add(new Point2D.Double(1300, 200));

        players = new TreeMap<>(new Comparator<Player>()
        {
            @Override public int compare(Player player, Player t1)
            {
                if( player.getPlayerNumber() == t1.getPlayerNumber() )
                {
                    return 0;
                }
                else if( player.getPlayerNumber() > t1.getPlayerNumber() )
                {
                    return 1;
                }
                else
                {
                    return -1;
                }

            }
        });
        //            playersList.sort(  );

        //teams
        this.team1 = new ArrayList<>();
        this.team2 = new ArrayList<>();
    }


    /**
     * @param team when there are no teams set this param is 0
     * @return
     */
    public Player addPlayer(int team, int id)
    {
        try
        {
            Player player = null;
            Aim aim = null;

            switch( players.size() )
            {
                case 0:
                    player = new Player(players.size(), 50, 500, ImageIO.read(new File("res/player0/rest.png")), 0, 1, this);
                    aim = new Aim(-1, -1, ImageIO.read(new File("res/player0/aim.png")), 0, 1, this);
                    player.setLevelSizes(arenaController.getLevelController().getLevelWidth(), arenaController.getLevelController().getLevelHeight());
                    players.put(player, aim);
                    player.setAim(aim);
                    player.setPlayerNumber(0);
                    player.setPlayerDatabaseId(id);
                    break;
                case 1:
                    player = new Player(players.size(), 250, 50, ImageIO.read(new File("res/player1/rest.png")), 0, 1, this);
                    aim = new Aim(-1, -1, ImageIO.read(new File("res/player1/aim.png")), 0, 1, this);
                    player.setLevelSizes(arenaController.getLevelController().getLevelWidth(), arenaController.getLevelController().getLevelHeight());
                    players.put(player, aim);
                    player.setAim(aim);
                    player.setPlayerNumber(1);
                    player.setPlayerDatabaseId(id);
                    break;
                case 2:
                    player = new Player(players.size(), 1120, 525, ImageIO.read(new File("res/player2/rest.png")), 0, 1, this);
                    aim = new Aim(-1, -1, ImageIO.read(new File("res/player2/aim.png")), 0, 1, this);
                    player.setLevelSizes(arenaController.getLevelController().getLevelWidth(), arenaController.getLevelController().getLevelHeight());
                    players.put(player, aim);
                    player.setAim(aim);
                    player.setPlayerNumber(2);
                    player.setPlayerDatabaseId(id);
                    break;
                case 3:
                    player = new Player(players.size(), 1300, 200, ImageIO.read(new File("res/player3/rest.png")), 0, 1, this);
                    aim = new Aim(-1, -1, ImageIO.read(new File("res/player3/aim.png")), 0, 1, this);
                    player.setLevelSizes(arenaController.getLevelController().getLevelWidth(), arenaController.getLevelController().getLevelHeight());
                    players.put(player, aim);
                    player.setAim(aim);
                    player.setPlayerNumber(3);
                    player.setPlayerDatabaseId(id);
                    break;
                default:
                    break;
            }
            if( team != 0 )
            {
                if( team == 1 )
                {
                    this.team1.add(player);
                }
                else
                {
                    this.team2.add(player);
                }
            }
            player.setTeam(team);

            //print teams
            System.out.println("team1");
            this.team1.forEach(e -> System.out.println(e));

            System.out.println("team2");
            this.team2.forEach(e -> System.out.println(e));

            setSpawnPoint(player);
            return player;
        }
        catch( IOException e )
        {
            e.printStackTrace();
        }
        return null;
    }

    public void setSpawnPoint(Player player)
    {

        player.setPosition(spawnPoints.get(players.keySet().size() - 1));

    }

    public List<Draw> getDrawObjects()
    {
        ArrayList<Draw> allObjects = new ArrayList<>();
        allObjects.addAll(drawObjects);

        //only living players
        List<Player> list = players.keySet().stream().filter(e -> !e.isDead()).collect(Collectors.toList());
        allObjects.addAll(list);

        for( Player player : list )
        {
            allObjects.add(players.get(player));
        }

        //		allObjects.addAll(players.keySet());
        //		allObjects.addAll(players.values());
        allObjects.addAll(this.generalArrows);
        return allObjects;
    }

    public List<Draw> getCollissionObjects()
    {
        ArrayList<Draw> allObjects = new ArrayList<>();
        allObjects.addAll(drawObjects);
        allObjects.addAll(players.keySet());
        return allObjects;

    }

    public void movePlayer(char e, int playerNumber)
    {
        if( players.size() >= playerNumber )
        {
            List<Player> playersList = new ArrayList<>(players.keySet());
            Player p = playersList.get(playerNumber);

            switch( e )
            {
                case 'a':
                    p.walkLeft();
                    break;
                case 'd':
                    p.walkRight();
                    break;
                case 's':
                    p.chrouch();
                    break;
                case 'w':
                    p.jump();
                    break;
                case 'c':
                    p.chrouch();
                    break;
                case 'e':
                    p.rest();
                    break;
                case 'q':
                    p.fire();
                    break;
                default:
                    p.rest();
            }
            arenaController.getArenaView().repaint();
        }
    }

    public void resetPlayerMovement(char e, int playerNumber)
    {
        List<Player> playerList = new ArrayList<>(players.keySet());
        Player player = playerList.get(playerNumber);
        switch( e )
        {
            case 'a':
                player.resetwalkLeft();
                break;
            case 'd':
                player.resetwalkingRight();
                break;
            case 's':
                player.resetCrouching();
                break;
            case 'w':
                player.resetJumping();
                break;
            case 'c':
                break;
            case 'e':
                break;
        }
    }

    /**
     * @param aimpoint
     * @param playerNumber
     */
    public void aimPlayer(Point2D aimpoint, int playerNumber)
    {
        List<Player> playersList = new ArrayList<>(players.keySet());
        Player p = playersList.get(playerNumber);
        p.aim(aimpoint);
    }

    /**
     * sets the location of the aim this method is called by a player
     *
     * @param player number of the player (0 - 3)
     * @param aim    An aim object with new coordinates
     */
    public void setAim(Player player, Aim aim)
    {
        if( players.containsKey(player) )
        {
            players.put(player, aim);
        }
    }

    public Aim getAim(Player player)
    {

        return players.get(player);

    }

    public ArenaController getController()
    {
        return arenaController;
    }

    public void setController(ArenaController controller)
    {
        arenaController = controller;
    }

    /**
     * return the scores of both teams.
     *
     * @return the scores of both teams, index 0 = team1, index 1 = team2.
     */
    public int[] getTeamScores()
    {
        int[] scores = {0, 0};
        this.getTeam1().forEach(player -> scores[0] += player.getKills());
        this.getTeam2().forEach(player -> scores[1] += player.getKills());
        return scores;
    }

    public Map<Player, Aim> getPlayersMap()
    {
        return players;
    }

    public void setPlayersMap(Map<Player, Aim> playersMap)
    {
        this.players = playersMap;
    }

    public ArenaController getArenaController()
    {
        return arenaController;
    }

    public void addArrowsToGeneralArrows(List<Arrow> killedPlayerArrows)
    {
        this.generalArrows.addAll(killedPlayerArrows);
    }

    public List<Arrow> getGeneralArrows()
    {
        return generalArrows;
    }

    public List<Player> getTeam1()
    {
        return team1;
    }

    public List<Player> getTeam2()
    {
        return team2;
    }

    public void resetGeneralArrows()
    {
        this.generalArrows = new ArrayList<>();
    }

    public ArrayList<BufferedImage> getPlayerfaces()
    {
        return playerfaces;
    }

    public void setPlayerfaces(ArrayList<BufferedImage> playerfaces)
    {
        this.playerfaces = playerfaces;
    }
}
