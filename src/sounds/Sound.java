/* Copyright (c) 2015 Davey Mathijssen */
package sounds;

import options.SoundOptions;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.applet.Applet;
import java.applet.AudioClip;

public final class Sound
{

    private final static AudioClip arrowHitFloor = Applet.newAudioClip(Sound.class.getResource("floor_hit_arrow.wav"));
    private final static AudioClip playerHitByArrow = Applet.newAudioClip(Sound.class.getResource("player_hit_arrow.wav"));
    private final static AudioClip shootArrow = Applet.newAudioClip(Sound.class.getResource("shoot_arrow.wav"));
    private final static AudioClip collectArrow = Applet.newAudioClip(Sound.class.getResource("collect_arrow.wav"));
    private final static AudioClip gameTheme = Applet.newAudioClip(Sound.class.getResource("gameTheme.wav"));
    private final static AudioClip playerJump = Applet.newAudioClip(Sound.class.getResource("player_jump.wav"));

    public static void playFloorHit()
    {
        if( SoundOptions.soundEffectOn )
        {
            arrowHitFloor.play();
        }
    }

    public static void playPlayerJump()
    {
        if( SoundOptions.soundEffectOn )
        {
            playerJump.play();
        }
    }

    public static void playPlayerHit()
    {
        if( SoundOptions.soundEffectOn )
        {
            playerHitByArrow.play();
        }
    }

    public static void playShootArrow()
    {
        if( SoundOptions.soundEffectOn )
        {
            shootArrow.play();
        }
    }

    public static void playGameTheme()
    {
        if( SoundOptions.musicOn )
        {
            gameTheme.loop();
//            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(getClass().getResource("explosion.wav"));
//            Clip clip = AudioSystem.getClip();
//            clip.open(audioInputStream);
//            clip.start();
        }
    }

    public static void stopGameTheme()
    {
        gameTheme.stop();
    }

    public static void playCollectArrow()
    {
        if( SoundOptions.soundEffectOn )
        {
            collectArrow.play();
        }
    }
}